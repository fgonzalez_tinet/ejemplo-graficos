package cl.wom.portabilidadejecutiva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * Application.
 *
 * @author Kendru Estrada.
 */
@SpringBootApplication
public class Application {


    /**
     * 
     * Metodo main.
     *
     * @param args argumentos.
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

