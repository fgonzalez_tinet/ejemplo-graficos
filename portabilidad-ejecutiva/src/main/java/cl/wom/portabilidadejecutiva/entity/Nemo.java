package cl.wom.portabilidadejecutiva.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Nemo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private long id;

	private String nombreNemo;

	@OneToMany
	@JoinColumn(name = "idNemo", referencedColumnName = "id")
	private List<DetalleNemo> detalleNemo;

	public List<DetalleNemo> getDetalleNemo() {
		return detalleNemo;
	}

	public void setDetalleNemo(List<DetalleNemo> detalleNemo) {
		this.detalleNemo = detalleNemo;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the nombreNemo
	 */
	public String getNombreNemo() {
		return nombreNemo;
	}

	/**
	 * @param nombreNemo
	 *            the nombreNemo to set
	 */
	public void setNombreNemo(String nombreNemo) {
		this.nombreNemo = nombreNemo;
	}

}
