package cl.wom.portabilidadejecutiva.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.wom.portabilidadejecutiva.entity.Nemo;

/**
 * Repositorio Spring Data JPA para la entidad NumeroPortabilidad.
 *
 * @author TInet - Juan Pablo Vega
 */
public interface NemoRepository extends JpaRepository<Nemo, Long> {

}
