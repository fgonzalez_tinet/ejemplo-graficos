package cl.wom.portabilidadejecutiva.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.wom.portabilidadejecutiva.entity.DetalleNemo;
import cl.wom.portabilidadejecutiva.entity.Nemo;
import cl.wom.portabilidadejecutiva.repository.NemoRepository;
import cl.wom.portabilidadejecutiva.to.NemoResponse;

/**
 * 
 * Controlador Rest para manejar el estado de la portabilidad.
 *
 * @author Oscar Saavedra H.
 */
@RestController
@RequestMapping("/acciones")
public class NemoController {

	/**
	 * Logger de la clase.
	 */
	private final Logger log = LoggerFactory.getLogger(NemoController.class);

	/**
	 * Repositorio JPA para la conexion con la base de datos.
	 */
	@Autowired
	NemoRepository nemoRepository;

	@RequestMapping(value = "/nemos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NemoResponse> obtenerNemos() {
		log.debug("[obtenerNemos] inicio obtención de Nemos");
		List<Nemo> nemos = nemoRepository.findAll();
		List<NemoResponse> nemoResponse = new ArrayList<NemoResponse>();
		if (nemos != null && !nemos.isEmpty()) {
			NemoResponse nemo;
			for (Nemo temp : nemos) {
				nemo = new NemoResponse();
				if (temp != null) {
					nemo.setNombreNemo(temp.getNombreNemo());
					if (temp.getDetalleNemo() != null) {
						StringBuffer detalle = new StringBuffer();
						detalle.append("[ ");
						for (DetalleNemo det : temp.getDetalleNemo()) {
							detalle.append("[" + det.getCategoria() + "," + det.getValor() + "]");
							if (!det.equals(temp.getDetalleNemo().get(temp.getDetalleNemo().size() - 1))) {
								detalle.append(",");
							}
						}
						detalle.append(" ]");
						nemo.setDetalleNemo(detalle.toString());
					}
					nemoResponse.add(nemo);
				}
			}
		}

		log.debug("[obtenerNemos] fin obtención de Nemos");
		return nemoResponse;

	}

	@RequestMapping(value = "/nemos/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Nemo obtenerNemo(@PathVariable long id) {
		log.debug("[obtenerNemo] inicio obtención de Nemos");
		log.debug("[obtenerNemo] fin obtención de Nemos");
		return nemoRepository.findOne(id);

	}

	@RequestMapping(value = "/nemosporid/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public NemoResponse obtenerNemoPorId(@PathVariable long id) {
		log.debug("[obtenerNemo] inicio obtención de Nemos");
		Nemo temp = nemoRepository.findOne(id);
		NemoResponse nemoResponse = new NemoResponse();
		if (temp != null) {
			nemoResponse.setNombreNemo(temp.getNombreNemo());
			if (temp.getDetalleNemo() != null) {
				StringBuffer detalle = new StringBuffer();
				detalle.append("[ ");
				for (DetalleNemo det : temp.getDetalleNemo()) {
					detalle.append("[" + det.getCategoria() + "," + det.getValor() + "]");
					if (!det.equals(temp.getDetalleNemo().get(temp.getDetalleNemo().size() - 1))) {
						detalle.append(",");
					}
				}
				detalle.append(" ]");
				nemoResponse.setDetalleNemo(detalle.toString());
			}
		}

		log.debug("[obtenerNemo] fin obtención de Nemos");
		return nemoResponse;
	}
}
