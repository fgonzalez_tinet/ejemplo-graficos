package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

/**
 * Clase que representa los datos de cliente obtenidos desde siebel.
 *
 * @author TInet - Juan Pablo Vega
 */
public class ClienteTO implements Serializable {

    /**
     * serial de la clase.
     */
    private static final long serialVersionUID = 7469872868399712618L;

    /**
     * companyName.
     */
    private String companyName;

    /**
     * rut.
     */
    private String rut;

    /**
     * billingCustomerCodeSiebel.
     */
    private String billingCustomerCodeSiebel;

    /**
     * crmCustomerCodeSiebel.
     */
    private String crmCustomerCodeSiebel;

    /**
     * integrationIdSiebel.
     */
    private String integrationIdSiebel;

    /**
     * ciudad.
     */
    private String ciudad;

    /**
     * pais.
     */
    private String pais;

    /**
     * numero.
     */
    private String numero;

    /**
     * codigoPostal.
     */
    private String codigoPostal;

    /**
     * estado.
     */
    private String estado;

    /**
     * calle.
     */
    private String calle;

    /**
     * nombre.
     */
    private String nombre;

    /**
     * apellido.
     */
    private String apellido;

    /**
     * cliente existe en siebel.
     */
    private boolean clienteSiebel;

    /**
     * estado del cliente.
     */
    private String customerStatus;

    /**
     * Permite obtener el valor del atributo companyName.
     * 
     * @return the companyName value.
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Permite establecer el valor del atributo companyName.
     *
     * @param companyName
     *            new value for companyName attribute.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Permite obtener el valor del atributo rut.
     * 
     * @return the rut value.
     */
    public String getRut() {
        return rut;
    }

    /**
     * Permite establecer el valor del atributo rut.
     *
     * @param rut
     *            new value for rut attribute.
     */
    public void setRut(String rut) {
        this.rut = rut;
    }

    /**
     * Permite obtener el valor del atributo billingCustomerCodeSiebel.
     * 
     * @return the billingCustomerCodeSiebel value.
     */
    public String getBillingCustomerCodeSiebel() {
        return billingCustomerCodeSiebel;
    }

    /**
     * Permite establecer el valor del atributo billingCustomerCodeSiebel.
     *
     * @param billingCustomerCodeSiebel
     *            new value for billingCustomerCodeSiebel attribute.
     */
    public void setBillingCustomerCodeSiebel(String billingCustomerCodeSiebel) {
        this.billingCustomerCodeSiebel = billingCustomerCodeSiebel;
    }

    /**
     * Permite obtener el valor del atributo crmCustomerCodeSiebel.
     * 
     * @return the crmCustomerCodeSiebel value.
     */
    public String getCrmCustomerCodeSiebel() {
        return crmCustomerCodeSiebel;
    }

    /**
     * Permite establecer el valor del atributo crmCustomerCodeSiebel.
     *
     * @param crmCustomerCodeSiebel
     *            new value for crmCustomerCodeSiebel attribute.
     */
    public void setCrmCustomerCodeSiebel(String crmCustomerCodeSiebel) {
        this.crmCustomerCodeSiebel = crmCustomerCodeSiebel;
    }

    /**
     * Permite obtener el valor del atributo integrationIdSiebel.
     * 
     * @return the integrationIdSiebel value.
     */
    public String getIntegrationIdSiebel() {
        return integrationIdSiebel;
    }

    /**
     * Permite establecer el valor del atributo integrationIdSiebel.
     *
     * @param integrationIdSiebel
     *            new value for integrationIdSiebel attribute.
     */
    public void setIntegrationIdSiebel(String integrationIdSiebel) {
        this.integrationIdSiebel = integrationIdSiebel;
    }

    /**
     * Permite obtener el valor del atributo ciudad.
     * 
     * @return the ciudad value.
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Permite establecer el valor del atributo ciudad.
     *
     * @param ciudad
     *            new value for ciudad attribute.
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Permite obtener el valor del atributo pais.
     * 
     * @return the pais value.
     */
    public String getPais() {
        return pais;
    }

    /**
     * Permite establecer el valor del atributo pais.
     *
     * @param pais
     *            new value for pais attribute.
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Permite obtener el valor del atributo numero.
     * 
     * @return the numero value.
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Permite establecer el valor del atributo numero.
     *
     * @param numero
     *            new value for numero attribute.
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Permite obtener el valor del atributo codigoPostal.
     * 
     * @return the codigoPostal value.
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Permite establecer el valor del atributo codigoPostal.
     *
     * @param codigoPostal
     *            new value for codigoPostal attribute.
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Permite obtener el valor del atributo estado.
     * 
     * @return the estado value.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Permite establecer el valor del atributo estado.
     *
     * @param estado
     *            new value for estado attribute.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Permite obtener el valor del atributo calle.
     * 
     * @return the calle value.
     */
    public String getCalle() {
        return calle;
    }

    /**
     * Permite establecer el valor del atributo calle.
     *
     * @param calle
     *            new value for calle attribute.
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * Permite obtener el valor del atributo nombre.
     * 
     * @return the nombre value.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Permite establecer el valor del atributo nombre.
     *
     * @param nombre
     *            new value for nombre attribute.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Permite obtener el valor del atributo apellido.
     * 
     * @return the apellido value.
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Permite establecer el valor del atributo apellido.
     *
     * @param apellido
     *            new value for apellido attribute.
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Permite obtener el valor del atributo clienteSiebel.
     * @return the clienteSiebel value.
     */
    public boolean isClienteSiebel() {
        return clienteSiebel;
    }

    /**
     * Permite establecer el valor del atributo clienteSiebel.
     *
     * @param clienteSiebel new value for clienteSiebel attribute.
     */
    public void setClienteSiebel(boolean clienteSiebel) {
        this.clienteSiebel = clienteSiebel;
    }

    /**
     * Permite obtener el valor del atributo customerStatus.
     * @return the customerStatus value.
     */
    public String getCustomerStatus() {
        return customerStatus;
    }

    /**
     * Permite establecer el valor del atributo customerStatus.
     *
     * @param customerStatus new value for customerStatus attribute.
     */
    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ClienteTO [companyName=").append(companyName)
                .append(", rut=").append(rut)
                .append(", billingCustomerCodeSiebel=")
                .append(billingCustomerCodeSiebel)
                .append(", crmCustomerCodeSiebel=")
                .append(crmCustomerCodeSiebel).append(", integrationIdSiebel=")
                .append(integrationIdSiebel).append(", ciudad=").append(ciudad)
                .append(", pais=").append(pais).append(", numero=")
                .append(numero).append(", codigoPostal=").append(codigoPostal)
                .append(", estado=").append(estado).append(", calle=")
                .append(calle).append(", nombre=").append(nombre)
                .append(", apellido=").append(apellido)
                .append(", clienteSiebel=").append(clienteSiebel)
                .append(", customerStatus=").append(customerStatus).append("]");
        return builder.toString();
    }

}
