package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase de transferencia para CoActivated.
 * 
 * @author TINet - Gustavo Arancibia V.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoActivatedTO implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Fecha.
     */
    @JsonProperty("date")
    private String date;

    /**
     * Permite obtener el valor del atributo date.
     * 
     * @return the date value.
     */
    public String getDate() {
        return date;
    }

    /**
     * Permite establecer el valor del atributo date.
     *
     * @param date
     *            new value for date attribute.
     */
    public void setDate(String date) {
        this.date = date;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CoActivatedTO [date=");
        builder.append(date);
        builder.append("]");
        return builder.toString();
    }

}
