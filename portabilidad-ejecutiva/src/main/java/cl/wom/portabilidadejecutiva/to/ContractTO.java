package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase de transferencia para Contract.
 * 
 * @author TINet - Gustavo Arancibia V.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractTO implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Ciudad.
     */
    @JsonProperty("adrCity")
    private String adrCity;

    /**
     * Apellido paterno.
     */
    @JsonProperty("adrFname")
    private String adrFname;

    /**
     * Apellido materno.
     */
    @JsonProperty("adrLname")
    private String adrLname;

    /**
     * Nombre.
     */
    @JsonProperty("adrName")
    private String adrName;

    /**
     * Calle.
     */
    @JsonProperty("adrStreet")
    private String adrStreet;

    /**
     * Numero de calle.
     */
    @JsonProperty("adrStreetno")
    private String adrStreetno;

    /**
     * Codigo ZIP.
     */
    @JsonProperty("adrZip")
    private String adrZip;

    /**
     * BuId.
     */
    @JsonProperty("buId")
    private Long buId;

    /**
     * CoActivated.
     */
    @JsonProperty("coActivated")
    private CoActivatedTO coActivated;

    /**
     * CoId.
     */
    @JsonProperty("coId")
    private Long coId;

    /**
     * CoIdPub.
     */
    @JsonProperty("coIdPub")
    private String coIdPub;

    /**
     * CoLec.
     */
    @JsonProperty("coLec")
    private Long coLec;

    /**
     * CoStatus.
     */
    @JsonProperty("coStatus")
    private Integer coStatus;

    /**
     * CoType.
     */
    @JsonProperty("coType")
    private String coType;

    /**
     * Identificador de tipo de contrato.
     */
    @JsonProperty("contractTypeId")
    private Long contractTypeId;

    /**
     * CsCode.
     */
    @JsonProperty("csCode")
    private String csCode;

    /**
     * CsId.
     */
    @JsonProperty("csId")
    private Long csId;

    /**
     * CsIdPub.
     */
    @JsonProperty("csIdPub")
    private String csIdPub;

    /**
     * CurrentDn.
     */
    @JsonProperty("currentDn")
    private Boolean currentDn;

    /**
     * Descripcion.
     */
    @JsonProperty("description")
    private String description;

    /**
     * DevPortNum.
     */
    @JsonProperty("devPortNum")
    private String devPortNum;

    /**
     * Dirnum.
     */
    @JsonProperty("dirnum")
    private String dirnum;

    /**
     * DirnumBlocks.
     */
    @JsonProperty("dirnumBlocks")
    private ListaDirnumBlocksTO dirnumBlocks;

    /**
     * Externalind.
     */
    @JsonProperty("externalind")
    private Boolean externalind;

    /**
     * ForeignBidGroupId.
     */
    @JsonProperty("foreignBidGroupId")
    private Long foreignBidGroupId;

    /**
     * ForeignPlCode.
     */
    @JsonProperty("foreignPlcode")
    private Long foreignPlcode;

    /**
     * HomeBidGroupId.
     */
    @JsonProperty("homeBidGroupId")
    private Long homeBidGroupId;

    /**
     * MainCoId.
     */
    @JsonProperty("mainCoId")
    private Long mainCoId;

    /**
     * MainCoIdPub.
     */
    @JsonProperty("mainCoIdPub")
    private String mainCoIdPub;

    /**
     * Plcode.
     */
    @JsonProperty("plcode")
    private Long plcode;

    /**
     * PortNum.
     */
    @JsonProperty("portNum")
    private String portNum;

    /**
     * Rpcode.
     */
    @JsonProperty("rpcode")
    private Long rpcode;

    /**
     * SmNum.
     */
    @JsonProperty("smNum")
    private String smNum;

    /**
     * SubmId.
     */
    @JsonProperty("submId")
    private Long submId;

    /**
     * VpnId.
     */
    @JsonProperty("vpnId")
    private Long vpnId;

    /**
     * Permite obtener el valor del atributo adrCity.
     * 
     * @return the adrCity value.
     */
    public String getAdrCity() {
        return adrCity;
    }

    /**
     * Permite establecer el valor del atributo adrCity.
     *
     * @param adrCity
     *            new value for adrCity attribute.
     */
    public void setAdrCity(String adrCity) {
        this.adrCity = adrCity;
    }

    /**
     * Permite obtener el valor del atributo adrFname.
     * 
     * @return the adrFname value.
     */
    public String getAdrFname() {
        return adrFname;
    }

    /**
     * Permite establecer el valor del atributo adrFname.
     *
     * @param adrFname
     *            new value for adrFname attribute.
     */
    public void setAdrFname(String adrFname) {
        this.adrFname = adrFname;
    }

    /**
     * Permite obtener el valor del atributo adrLname.
     * 
     * @return the adrLname value.
     */
    public String getAdrLname() {
        return adrLname;
    }

    /**
     * Permite establecer el valor del atributo adrLname.
     *
     * @param adrLname
     *            new value for adrLname attribute.
     */
    public void setAdrLname(String adrLname) {
        this.adrLname = adrLname;
    }

    /**
     * Permite obtener el valor del atributo adrName.
     * 
     * @return the adrName value.
     */
    public String getAdrName() {
        return adrName;
    }

    /**
     * Permite establecer el valor del atributo adrName.
     *
     * @param adrName
     *            new value for adrName attribute.
     */
    public void setAdrName(String adrName) {
        this.adrName = adrName;
    }

    /**
     * Permite obtener el valor del atributo adrStreet.
     * 
     * @return the adrStreet value.
     */
    public String getAdrStreet() {
        return adrStreet;
    }

    /**
     * Permite establecer el valor del atributo adrStreet.
     *
     * @param adrStreet
     *            new value for adrStreet attribute.
     */
    public void setAdrStreet(String adrStreet) {
        this.adrStreet = adrStreet;
    }

    /**
     * Permite obtener el valor del atributo adrStreetno.
     * 
     * @return the adrStreetno value.
     */
    public String getAdrStreetno() {
        return adrStreetno;
    }

    /**
     * Permite establecer el valor del atributo adrStreetno.
     *
     * @param adrStreetno
     *            new value for adrStreetno attribute.
     */
    public void setAdrStreetno(String adrStreetno) {
        this.adrStreetno = adrStreetno;
    }

    /**
     * Permite obtener el valor del atributo adrZip.
     * 
     * @return the adrZip value.
     */
    public String getAdrZip() {
        return adrZip;
    }

    /**
     * Permite establecer el valor del atributo adrZip.
     *
     * @param adrZip
     *            new value for adrZip attribute.
     */
    public void setAdrZip(String adrZip) {
        this.adrZip = adrZip;
    }

    /**
     * Permite obtener el valor del atributo buId.
     * 
     * @return the buId value.
     */
    public Long getBuId() {
        return buId;
    }

    /**
     * Permite establecer el valor del atributo buId.
     *
     * @param buId
     *            new value for buId attribute.
     */
    public void setBuId(Long buId) {
        this.buId = buId;
    }

    /**
     * Permite obtener el valor del atributo coActivated.
     * 
     * @return the coActivated value.
     */
    public CoActivatedTO getCoActivated() {
        return coActivated;
    }

    /**
     * Permite establecer el valor del atributo coActivated.
     *
     * @param coActivated
     *            new value for coActivated attribute.
     */
    public void setCoActivated(CoActivatedTO coActivated) {
        this.coActivated = coActivated;
    }

    /**
     * Permite obtener el valor del atributo coId.
     * 
     * @return the coId value.
     */
    public Long getCoId() {
        return coId;
    }

    /**
     * Permite establecer el valor del atributo coId.
     *
     * @param coId
     *            new value for coId attribute.
     */
    public void setCoId(Long coId) {
        this.coId = coId;
    }

    /**
     * Permite obtener el valor del atributo coIdPub.
     * 
     * @return the coIdPub value.
     */
    public String getCoIdPub() {
        return coIdPub;
    }

    /**
     * Permite establecer el valor del atributo coIdPub.
     *
     * @param coIdPub
     *            new value for coIdPub attribute.
     */
    public void setCoIdPub(String coIdPub) {
        this.coIdPub = coIdPub;
    }

    /**
     * Permite obtener el valor del atributo coLec.
     * 
     * @return the coLec value.
     */
    public Long getCoLec() {
        return coLec;
    }

    /**
     * Permite establecer el valor del atributo coLec.
     *
     * @param coLec
     *            new value for coLec attribute.
     */
    public void setCoLec(Long coLec) {
        this.coLec = coLec;
    }

    /**
     * Permite obtener el valor del atributo coStatus.
     * 
     * @return the coStatus value.
     */
    public Integer getCoStatus() {
        return coStatus;
    }

    /**
     * Permite establecer el valor del atributo coStatus.
     *
     * @param coStatus
     *            new value for coStatus attribute.
     */
    public void setCoStatus(Integer coStatus) {
        this.coStatus = coStatus;
    }

    /**
     * Permite obtener el valor del atributo coType.
     * 
     * @return the coType value.
     */
    public String getCoType() {
        return coType;
    }

    /**
     * Permite establecer el valor del atributo coType.
     *
     * @param coType
     *            new value for coType attribute.
     */
    public void setCoType(String coType) {
        this.coType = coType;
    }

    /**
     * Permite obtener el valor del atributo contractTypeId.
     * 
     * @return the contractTypeId value.
     */
    public Long getContractTypeId() {
        return contractTypeId;
    }

    /**
     * Permite establecer el valor del atributo contractTypeId.
     *
     * @param contractTypeId
     *            new value for contractTypeId attribute.
     */
    public void setContractTypeId(Long contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    /**
     * Permite obtener el valor del atributo csCode.
     * 
     * @return the csCode value.
     */
    public String getCsCode() {
        return csCode;
    }

    /**
     * Permite establecer el valor del atributo csCode.
     *
     * @param csCode
     *            new value for csCode attribute.
     */
    public void setCsCode(String csCode) {
        this.csCode = csCode;
    }

    /**
     * Permite obtener el valor del atributo csId.
     * 
     * @return the csId value.
     */
    public Long getCsId() {
        return csId;
    }

    /**
     * Permite establecer el valor del atributo csId.
     *
     * @param csId
     *            new value for csId attribute.
     */
    public void setCsId(Long csId) {
        this.csId = csId;
    }

    /**
     * Permite obtener el valor del atributo csIdPub.
     * 
     * @return the csIdPub value.
     */
    public String getCsIdPub() {
        return csIdPub;
    }

    /**
     * Permite establecer el valor del atributo csIdPub.
     *
     * @param csIdPub
     *            new value for csIdPub attribute.
     */
    public void setCsIdPub(String csIdPub) {
        this.csIdPub = csIdPub;
    }

    /**
     * Permite obtener el valor del atributo currentDn.
     * 
     * @return the currentDn value.
     */
    public Boolean getCurrentDn() {
        return currentDn;
    }

    /**
     * Permite establecer el valor del atributo currentDn.
     *
     * @param currentDn
     *            new value for currentDn attribute.
     */
    public void setCurrentDn(Boolean currentDn) {
        this.currentDn = currentDn;
    }

    /**
     * Permite obtener el valor del atributo description.
     * 
     * @return the description value.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Permite establecer el valor del atributo description.
     *
     * @param description
     *            new value for description attribute.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Permite obtener el valor del atributo devPortNum.
     * 
     * @return the devPortNum value.
     */
    public String getDevPortNum() {
        return devPortNum;
    }

    /**
     * Permite establecer el valor del atributo devPortNum.
     *
     * @param devPortNum
     *            new value for devPortNum attribute.
     */
    public void setDevPortNum(String devPortNum) {
        this.devPortNum = devPortNum;
    }

    /**
     * Permite obtener el valor del atributo dirnum.
     * 
     * @return the dirnum value.
     */
    public String getDirnum() {
        return dirnum;
    }

    /**
     * Permite establecer el valor del atributo dirnum.
     *
     * @param dirnum
     *            new value for dirnum attribute.
     */
    public void setDirnum(String dirnum) {
        this.dirnum = dirnum;
    }

    /**
     * Permite obtener el valor del atributo dirnumBlocks.
     * 
     * @return the dirnumBlocks value.
     */
    public ListaDirnumBlocksTO getDirnumBlocks() {
        return dirnumBlocks;
    }

    /**
     * Permite establecer el valor del atributo dirnumBlocks.
     *
     * @param dirnumBlocks
     *            new value for dirnumBlocks attribute.
     */
    public void setDirnumBlocks(ListaDirnumBlocksTO dirnumBlocks) {
        this.dirnumBlocks = dirnumBlocks;
    }

    /**
     * Permite obtener el valor del atributo externalind.
     * 
     * @return the externalind value.
     */
    public Boolean getExternalind() {
        return externalind;
    }

    /**
     * Permite establecer el valor del atributo externalind.
     *
     * @param externalind
     *            new value for externalind attribute.
     */
    public void setExternalind(Boolean externalind) {
        this.externalind = externalind;
    }

    /**
     * Permite obtener el valor del atributo foreignBidGroupId.
     * 
     * @return the foreignBidGroupId value.
     */
    public Long getForeignBidGroupId() {
        return foreignBidGroupId;
    }

    /**
     * Permite establecer el valor del atributo foreignBidGroupId.
     *
     * @param foreignBidGroupId
     *            new value for foreignBidGroupId attribute.
     */
    public void setForeignBidGroupId(Long foreignBidGroupId) {
        this.foreignBidGroupId = foreignBidGroupId;
    }

    /**
     * Permite obtener el valor del atributo foreignPlcode.
     * 
     * @return the foreignPlcode value.
     */
    public Long getForeignPlcode() {
        return foreignPlcode;
    }

    /**
     * Permite establecer el valor del atributo foreignPlcode.
     *
     * @param foreignPlcode
     *            new value for foreignPlcode attribute.
     */
    public void setForeignPlcode(Long foreignPlcode) {
        this.foreignPlcode = foreignPlcode;
    }

    /**
     * Permite obtener el valor del atributo homeBidGroupId.
     * 
     * @return the homeBidGroupId value.
     */
    public Long getHomeBidGroupId() {
        return homeBidGroupId;
    }

    /**
     * Permite establecer el valor del atributo homeBidGroupId.
     *
     * @param homeBidGroupId
     *            new value for homeBidGroupId attribute.
     */
    public void setHomeBidGroupId(Long homeBidGroupId) {
        this.homeBidGroupId = homeBidGroupId;
    }

    /**
     * Permite obtener el valor del atributo mainCoId.
     * 
     * @return the mainCoId value.
     */
    public Long getMainCoId() {
        return mainCoId;
    }

    /**
     * Permite establecer el valor del atributo mainCoId.
     *
     * @param mainCoId
     *            new value for mainCoId attribute.
     */
    public void setMainCoId(Long mainCoId) {
        this.mainCoId = mainCoId;
    }

    /**
     * Permite obtener el valor del atributo mainCoIdPub.
     * 
     * @return the mainCoIdPub value.
     */
    public String getMainCoIdPub() {
        return mainCoIdPub;
    }

    /**
     * Permite establecer el valor del atributo mainCoIdPub.
     *
     * @param mainCoIdPub
     *            new value for mainCoIdPub attribute.
     */
    public void setMainCoIdPub(String mainCoIdPub) {
        this.mainCoIdPub = mainCoIdPub;
    }

    /**
     * Permite obtener el valor del atributo plcode.
     * 
     * @return the plcode value.
     */
    public Long getPlcode() {
        return plcode;
    }

    /**
     * Permite establecer el valor del atributo plcode.
     *
     * @param plcode
     *            new value for plcode attribute.
     */
    public void setPlcode(Long plcode) {
        this.plcode = plcode;
    }

    /**
     * Permite obtener el valor del atributo portNum.
     * 
     * @return the portNum value.
     */
    public String getPortNum() {
        return portNum;
    }

    /**
     * Permite establecer el valor del atributo portNum.
     *
     * @param portNum
     *            new value for portNum attribute.
     */
    public void setPortNum(String portNum) {
        this.portNum = portNum;
    }

    /**
     * Permite obtener el valor del atributo rpcode.
     * 
     * @return the rpcode value.
     */
    public Long getRpcode() {
        return rpcode;
    }

    /**
     * Permite establecer el valor del atributo rpcode.
     *
     * @param rpcode
     *            new value for rpcode attribute.
     */
    public void setRpcode(Long rpcode) {
        this.rpcode = rpcode;
    }

    /**
     * Permite obtener el valor del atributo smNum.
     * 
     * @return the smNum value.
     */
    public String getSmNum() {
        return smNum;
    }

    /**
     * Permite establecer el valor del atributo smNum.
     *
     * @param smNum
     *            new value for smNum attribute.
     */
    public void setSmNum(String smNum) {
        this.smNum = smNum;
    }

    /**
     * Permite obtener el valor del atributo submId.
     * 
     * @return the submId value.
     */
    public Long getSubmId() {
        return submId;
    }

    /**
     * Permite establecer el valor del atributo submId.
     *
     * @param submId
     *            new value for submId attribute.
     */
    public void setSubmId(Long submId) {
        this.submId = submId;
    }

    /**
     * Permite obtener el valor del atributo vpnId.
     * 
     * @return the vpnId value.
     */
    public Long getVpnId() {
        return vpnId;
    }

    /**
     * Permite establecer el valor del atributo vpnId.
     *
     * @param vpnId
     *            new value for vpnId attribute.
     */
    public void setVpnId(Long vpnId) {
        this.vpnId = vpnId;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ContractTO [adrCity=");
        builder.append(adrCity);
        builder.append(", adrFname=");
        builder.append(adrFname);
        builder.append(", adrLname=");
        builder.append(adrLname);
        builder.append(", adrName=");
        builder.append(adrName);
        builder.append(", adrStreet=");
        builder.append(adrStreet);
        builder.append(", adrStreetno=");
        builder.append(adrStreetno);
        builder.append(", adrZip=");
        builder.append(adrZip);
        builder.append(", buId=");
        builder.append(buId);
        builder.append(", coActivated=");
        builder.append(coActivated);
        builder.append(", coId=");
        builder.append(coId);
        builder.append(", coIdPub=");
        builder.append(coIdPub);
        builder.append(", coLec=");
        builder.append(coLec);
        builder.append(", coStatus=");
        builder.append(coStatus);
        builder.append(", coType=");
        builder.append(coType);
        builder.append(", contractTypeId=");
        builder.append(contractTypeId);
        builder.append(", csCode=");
        builder.append(csCode);
        builder.append(", csId=");
        builder.append(csId);
        builder.append(", csIdPub=");
        builder.append(csIdPub);
        builder.append(", currentDn=");
        builder.append(currentDn);
        builder.append(", description=");
        builder.append(description);
        builder.append(", devPortNum=");
        builder.append(devPortNum);
        builder.append(", dirnum=");
        builder.append(dirnum);
        builder.append(", dirnumBlocks=");
        builder.append(dirnumBlocks);
        builder.append(", externalind=");
        builder.append(externalind);
        builder.append(", foreignBidGroupId=");
        builder.append(foreignBidGroupId);
        builder.append(", foreignPlcode=");
        builder.append(foreignPlcode);
        builder.append(", homeBidGroupId=");
        builder.append(homeBidGroupId);
        builder.append(", mainCoId=");
        builder.append(mainCoId);
        builder.append(", mainCoIdPub=");
        builder.append(mainCoIdPub);
        builder.append(", plcode=");
        builder.append(plcode);
        builder.append(", portNum=");
        builder.append(portNum);
        builder.append(", rpcode=");
        builder.append(rpcode);
        builder.append(", smNum=");
        builder.append(smNum);
        builder.append(", submId=");
        builder.append(submId);
        builder.append(", vpnId=");
        builder.append(vpnId);
        builder.append("]");
        return builder.toString();
    }

}
