package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase de transferencia de ContractsSearchReturn.
 * 
 * @author TINet - Gustavo Arancibia V.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractsSearchReturnTO implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Lista de contratos.
     */
    @JsonProperty("contracts")
    private ListaContractsTO contracts;

    /**
     * Busqueda esta completa.
     */
    @JsonProperty("searchIsComplete")
    private Boolean searchIsComplete;

    /**
     * Permite obtener el valor del atributo contracts.
     * 
     * @return the contracts value.
     */
    public ListaContractsTO getContracts() {
        return contracts;
    }

    /**
     * Permite establecer el valor del atributo contracts.
     *
     * @param contracts
     *            new value for contracts attribute.
     */
    public void setContracts(ListaContractsTO contracts) {
        this.contracts = contracts;
    }

    /**
     * Permite obtener el valor del atributo searchIsComplete.
     * 
     * @return the searchIsComplete value.
     */
    public Boolean getSearchIsComplete() {
        return searchIsComplete;
    }

    /**
     * Permite establecer el valor del atributo searchIsComplete.
     *
     * @param searchIsComplete
     *            new value for searchIsComplete attribute.
     */
    public void setSearchIsComplete(Boolean searchIsComplete) {
        this.searchIsComplete = searchIsComplete;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ContractsSearchReturnTO [contracts=");
        builder.append(contracts);
        builder.append(", searchIsComplete=");
        builder.append(searchIsComplete);
        builder.append("]");
        return builder.toString();
    }

}
