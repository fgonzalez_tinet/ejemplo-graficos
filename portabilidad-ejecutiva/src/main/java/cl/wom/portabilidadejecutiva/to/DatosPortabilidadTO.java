package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

/**
 * Clase que representa todos los datos ingresados para realizar una
 * portabilidad.
 *
 * @author TInet - Juan Pablo Vega
 */
public class DatosPortabilidadTO implements Serializable {

    /**
     * Serial Version.
     */
    private static final long serialVersionUID = -8241117134763391188L;

    /**
     * Id de la solicitud.
     */
    private String idSolicitud;

    /**
     * compañia de origen.
     */
    private OperadoraTO compania;

    /**
     * atributo que representa el número de teléfono.
     */
    private String numeroTel;

    /**
     * atributo que representa el imei de origen.
     */
    private String imei;

    /**
     * atributo que representa el chip de WOM.
     */
    private String chipWom;

    /**
     * atributo que representa el rut del cliente.
     */
    private String rutCliente;

    /**
     * atributo que representa el número de serie del cliente.
     */
    private String serieCiCliente;

    /**
     * atributo que representa el nombre del cliente.
     */
    private String nombreCliente;

    /**
     * atributo que representa el apellido del cliente.
     */
    private String apellidoCliente;

    /**
     * atributo que representa el email del cliente.
     */
    private String emailCliente;

    /**
     * atributo que representa el teléfono de contacto del cliente.
     */
    private String telContacto;

    /**
     * atributo que representa la region del cliente.
     */
    private String region;

    /**
     * atributo que representa la descripcion de la region.
     */
    private String regionDesc;

    /**
     * atributo que representa la comuna del cliente.
     */
    private String comuna;

    /**
     * atributo que representa la descripcion de la comuna.
     */
    private String comunaDesc;

    /**
     * atributo que representa el nombre de la foto frontal.
     */
    private String fotoFrontal;

    /**
     * atributo que representa el nombre de la foto trasera.
     */
    private String fotoTrasera;

    /**
     * atributo que representa la provincia.
     */
    private String provincia;

    /**
     * atributo que representa la descripcion de la provincia.
     */
    private String provinciaDesc;

    /**
     * rpCode.
     */
    private String rpCode;

    /**
     * coId.
     */
    private String coId;

    /**
     * numeroNuevo.
     */
    private String numeroNuevo;

    /**
     * numeroPortar.
     */
    private String numeroPortar;

    /**
     * Codigo cap.
     */
    private String codigoCap;

    /**
     * datos de cliente.
     */
    private ClienteTO datosCliente;

/**
     * direccion punto.
     */
    private String direccionPunto;

    /**
     * codigo cliente.
     */
    private String codigoCliente;

    /**
     * requestCliente.
     */
    private String requestCliente;

    /**
     * requestOrder.
     */
    private String requestOrder;

    /**
     * estado.
     */
    private int estado;

    /**
     * paso.
     */
    private int paso;

    /**
     * usuario que hace la portabilidad.
     */
    private String nombreEjecutivo;

    /**
     * método que retorna la concatenacióon de todos los atributos
     * para ser guardados en base de datos.
     *
     * @return Concatenación de atributos de la clase.
     */
    public String getDatosConcatenados() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("companiaOrigen=");
        if (compania != null) {
            buffer.append(compania.getNombre());
        } else {
            buffer.append("null");
        }
        buffer.append(";").append("numeroTel=").append(numeroTel).append(";");
        buffer.append("imei=").append(imei).append(";");
        buffer.append("chipWom=").append(chipWom).append(";");
        buffer.append("rutCliente=").append(rutCliente).append(";");
        buffer.append("serieCiCliente=").append(serieCiCliente).append(";");
        buffer.append("nombreCliente=").append(nombreCliente).append(";");
        buffer.append("apellidoCliente=").append(apellidoCliente).append(";");
        buffer.append("emailCliente=").append(emailCliente).append(";");
        buffer.append("telContacto=").append(telContacto).append(";");
        buffer.append("region=").append(region).append(";");
        buffer.append("comuna=").append(comuna).append(";");
        buffer.append("fotoFrontal=");
        if (fotoFrontal != null && !fotoFrontal.isEmpty()) {
            buffer.append("existe;");
        } else {
            buffer.append("noExiste;");
        }
        buffer.append("fotoTrasera=");
        if (fotoTrasera != null && !fotoTrasera.isEmpty()) {
            buffer.append("existe;");
        } else {
            buffer.append("noExiste;");
        }
        return buffer.toString();
    }

    /**
     * Permite obtener el valor del atributo idSolicitud.
     * @return the idSolicitud value.
     */
    public String getIdSolicitud() {
        return idSolicitud;
    }

    /**
     * Permite establecer el valor del atributo idSolicitud.
     *
     * @param idSolicitud new value for idSolicitud attribute.
     */
    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    /**
     * Permite obtener el valor del atributo numeroTel.
     * @return the numeroTel value.
     */
    public String getNumeroTel() {
        return numeroTel;
    }

    /**
     * Permite establecer el valor del atributo numeroTel.
     *
     * @param numeroTel new value for numeroTel attribute.
     */
    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    /**
     * Permite obtener el valor del atributo imei.
     * @return the imei value.
     */
    public String getImei() {
        return imei;
    }

    /**
     * Permite establecer el valor del atributo imei.
     *
     * @param imei new value for imei attribute.
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * Permite obtener el valor del atributo chipWom.
     * @return the chipWom value.
     */
    public String getChipWom() {
        return chipWom;
    }

    /**
     * Permite establecer el valor del atributo chipWom.
     *
     * @param chipWom new value for chipWom attribute.
     */
    public void setChipWom(String chipWom) {
        this.chipWom = chipWom;
    }

    /**
     * Permite obtener el valor del atributo rutCliente.
     * @return the rutCliente value.
     */
    public String getRutCliente() {
        return rutCliente;
    }

    /**
     * Permite establecer el valor del atributo rutCliente.
     *
     * @param rutCliente new value for rutCliente attribute.
     */
    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    /**
     * Permite obtener el valor del atributo serieCiCliente.
     * @return the serieCiCliente value.
     */
    public String getSerieCiCliente() {
        return serieCiCliente;
    }

    /**
     * Permite establecer el valor del atributo serieCiCliente.
     *
     * @param serieCiCliente new value for serieCiCliente attribute.
     */
    public void setSerieCiCliente(String serieCiCliente) {
        this.serieCiCliente = serieCiCliente;
    }

    /**
     * Permite obtener el valor del atributo nombreCliente.
     * @return the nombreCliente value.
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Permite establecer el valor del atributo nombreCliente.
     *
     * @param nombreCliente new value for nombreCliente attribute.
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * Permite obtener el valor del atributo apellidoCliente.
     * @return the apellidoCliente value.
     */
    public String getApellidoCliente() {
        return apellidoCliente;
    }

    /**
     * Permite establecer el valor del atributo apellidoCliente.
     *
     * @param apellidoCliente new value for apellidoCliente attribute.
     */
    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    /**
     * Permite obtener el valor del atributo emailCliente.
     * @return the emailCliente value.
     */
    public String getEmailCliente() {
        return emailCliente;
    }

    /**
     * Permite establecer el valor del atributo emailCliente.
     *
     * @param emailCliente new value for emailCliente attribute.
     */
    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    /**
     * Permite obtener el valor del atributo telContacto.
     * @return the telContacto value.
     */
    public String getTelContacto() {
        return telContacto;
    }

    /**
     * Permite establecer el valor del atributo telContacto.
     *
     * @param telContacto new value for telContacto attribute.
     */
    public void setTelContacto(String telContacto) {
        this.telContacto = telContacto;
    }

    /**
     * Permite obtener el valor del atributo region.
     * @return the region value.
     */
    public String getRegion() {
        return region;
    }

    /**
     * Permite establecer el valor del atributo region.
     *
     * @param region new value for region attribute.
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * Permite obtener el valor del atributo comuna.
     * @return the comuna value.
     */
    public String getComuna() {
        return comuna;
    }

    /**
     * Permite establecer el valor del atributo comuna.
     *
     * @param comuna new value for comuna attribute.
     */
    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    /**
     * Permite obtener el valor del atributo fotoFrontal.
     * @return the fotoFrontal value.
     */
    public String getFotoFrontal() {
        return fotoFrontal;
    }

    /**
     * Permite establecer el valor del atributo fotoFrontal.
     *
     * @param fotoFrontal new value for fotoFrontal attribute.
     */
    public void setFotoFrontal(String fotoFrontal) {
        this.fotoFrontal = fotoFrontal;
    }

    /**
     * Permite obtener el valor del atributo fotoTrasera.
     * @return the fotoTrasera value.
     */
    public String getFotoTrasera() {
        return fotoTrasera;
    }

    /**
     * Permite establecer el valor del atributo fotoTrasera.
     *
     * @param fotoTrasera new value for fotoTrasera attribute.
     */
    public void setFotoTrasera(String fotoTrasera) {
        this.fotoTrasera = fotoTrasera;
    }

    /**
     * Permite obtener el valor del atributo rpCode.
     * @return the rpCode value.
     */
    public String getRpCode() {
        return rpCode;
    }

    /**
     * Permite establecer el valor del atributo rpCode.
     *
     * @param rpCode new value for rpCode attribute.
     */
    public void setRpCode(String rpCode) {
        this.rpCode = rpCode;
    }

    /**
     * Permite obtener el valor del atributo coId.
     * @return the coId value.
     */
    public String getCoId() {
        return coId;
    }

    /**
     * Permite establecer el valor del atributo coId.
     *
     * @param coId new value for coId attribute.
     */
    public void setCoId(String coId) {
        this.coId = coId;
    }

    /**
     * Permite obtener el valor del atributo numeroNuevo.
     * @return the numeroNuevo value.
     */
    public String getNumeroNuevo() {
        return numeroNuevo;
    }

    /**
     * Permite establecer el valor del atributo numeroNuevo.
     *
     * @param numeroNuevo new value for numeroNuevo attribute.
     */
    public void setNumeroNuevo(String numeroNuevo) {
        this.numeroNuevo = numeroNuevo;
    }

    /**
     * Permite obtener el valor del atributo numeroPortar.
     * @return the numeroPortar value.
     */
    public String getNumeroPortar() {
        return numeroPortar;
    }

    /**
     * Permite establecer el valor del atributo numeroPortar.
     *
     * @param numeroPortar new value for numeroPortar attribute.
     */
    public void setNumeroPortar(String numeroPortar) {
        this.numeroPortar = numeroPortar;
    }

    /**
     * Permite obtener el valor del atributo codigoCap.
     * @return the codigoCap value.
     */
    public String getCodigoCap() {
        return codigoCap;
    }

    /**
     * Permite establecer el valor del atributo codigoCap.
     *
     * @param codigoCap new value for codigoCap attribute.
     */
    public void setCodigoCap(String codigoCap) {
        this.codigoCap = codigoCap;
    }

    /**
     * Permite obtener el valor del atributo datosCliente.
     * @return the datosCliente value.
     */
    public ClienteTO getDatosCliente() {
        return datosCliente;
    }

    /**
     * Permite establecer el valor del atributo datosCliente.
     *
     * @param datosCliente new value for datosCliente attribute.
     */
    public void setDatosCliente(ClienteTO datosCliente) {
        this.datosCliente = datosCliente;
    }

    /**
     * Permite obtener el valor del atributo regionDesc.
     * @return the regionDesc value.
     */
    public String getRegionDesc() {
        return regionDesc;
    }

    /**
     * Permite establecer el valor del atributo regionDesc.
     *
     * @param regionDesc new value for regionDesc attribute.
     */
    public void setRegionDesc(String regionDesc) {
        this.regionDesc = regionDesc;
    }

    /**
     * Permite obtener el valor del atributo comunaDesc.
     * @return the comunaDesc value.
     */
    public String getComunaDesc() {
        return comunaDesc;
    }

    /**
     * Permite establecer el valor del atributo comunaDesc.
     *
     * @param comunaDesc new value for comunaDesc attribute.
     */
    public void setComunaDesc(String comunaDesc) {
        this.comunaDesc = comunaDesc;
    }

    /**
     * Permite obtener el valor del atributo provincia.
     * @return the provincia value.
     */
    public String getProvincia() {
        return provincia;
    }

    /**
     * Permite establecer el valor del atributo provincia.
     *
     * @param provincia new value for provincia attribute.
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    /**
     * Permite obtener el valor del atributo provinciaDesc.
     * @return the provinciaDesc value.
     */
    public String getProvinciaDesc() {
        return provinciaDesc;
    }

    /**
     * Permite establecer el valor del atributo provinciaDesc.
     *
     * @param provinciaDesc new value for provinciaDesc attribute.
     */
    public void setProvinciaDesc(String provinciaDesc) {
        this.provinciaDesc = provinciaDesc;
    }

    /**
     * Permite obtener el valor del atributo direccionPunto.
     * @return the direccionPunto value.
     */
    public String getDireccionPunto() {
        return direccionPunto;
    }

    /**
     * Permite establecer el valor del atributo direccionPunto.
     *
     * @param direccionPunto new value for direccionPunto attribute.
     */
    public void setDireccionPunto(String direccionPunto) {
        this.direccionPunto = direccionPunto;
    }

    /**
     * Permite obtener el valor del atributo codigoCliente.
     * @return the codigoCliente value.
     */
    public String getCodigoCliente() {
        return codigoCliente;
    }

    /**
     * Permite establecer el valor del atributo codigoCliente.
     *
     * @param codigoCliente new value for codigoCliente attribute.
     */
    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    /**
     * Permite obtener el valor del atributo requestCliente.
     * @return the requestCliente value.
     */
    public String getRequestCliente() {
        return requestCliente;
    }

    /**
     * Permite establecer el valor del atributo requestCliente.
     *
     * @param requestCliente new value for requestCliente attribute.
     */
    public void setRequestCliente(String requestCliente) {
        this.requestCliente = requestCliente;
    }

    /**
     * Permite obtener el valor del atributo requestOrder.
     * @return the requestOrder value.
     */
    public String getRequestOrder() {
        return requestOrder;
    }

    /**
     * Permite establecer el valor del atributo requestOrder.
     *
     * @param requestOrder new value for requestOrder attribute.
     */
    public void setRequestOrder(String requestOrder) {
        this.requestOrder = requestOrder;
    }

    /**
     * Permite obtener el valor del atributo compania.
     * @return the compania value.
     */
    public OperadoraTO getCompania() {
        return compania;
    }

    /**
     * Permite establecer el valor del atributo compania.
     *
     * @param compania new value for compania attribute.
     */
    public void setCompania(OperadoraTO compania) {
        this.compania = compania;
    }

    /**
     * Permite obtener el valor del atributo estado.
     * @return the estado value.
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Permite establecer el valor del atributo estado.
     *
     * @param estado new value for estado attribute.
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * Permite obtener el valor del atributo paso.
     * @return the paso value.
     */
    public int getPaso() {
        return paso;
    }

    /**
     * Permite establecer el valor del atributo paso.
     *
     * @param paso new value for paso attribute.
     */
    public void setPaso(int paso) {
        this.paso = paso;
    }

    /**
     * Permite obtener el valor del atributo nombreEjecutivo.
     * @return the nombreEjecutivo value.
     */
    public String getNombreEjecutivo() {
        return nombreEjecutivo;
    }

    /**
     * Permite establecer el valor del atributo nombreEjecutivo.
     *
     * @param nombreEjecutivo new value for nombreEjecutivo attribute.
     */
    public void setNombreEjecutivo(String nombreEjecutivo) {
        this.nombreEjecutivo = nombreEjecutivo;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DatosPortabilidadTO [idSolicitud=").append(idSolicitud)
                .append(", compania=").append(compania).append(", numeroTel=")
                .append(numeroTel).append(", imei=").append(imei)
                .append(", chipWom=").append(chipWom).append(", rutCliente=")
                .append(rutCliente).append(", serieCiCliente=")
                .append(serieCiCliente).append(", nombreCliente=")
                .append(nombreCliente).append(", apellidoCliente=")
                .append(apellidoCliente).append(", emailCliente=")
                .append(emailCliente).append(", telContacto=")
                .append(telContacto).append(", region=").append(region)
                .append(", regionDesc=").append(regionDesc).append(", comuna=")
                .append(comuna).append(", comunaDesc=").append(comunaDesc)
                .append(", fotoFrontal=").append(fotoFrontal)
                .append(", fotoTrasera=").append(fotoTrasera)
                .append(", provincia=").append(provincia)
                .append(", provinciaDesc=").append(provinciaDesc)
                .append(", rpCode=").append(rpCode).append(", coId=")
                .append(coId).append(", numeroNuevo=").append(numeroNuevo)
                .append(", numeroPortar=").append(numeroPortar)
                .append(", codigoCap=").append(codigoCap)
                .append(", datosCliente=").append(datosCliente)
                .append(", direccionPunto=").append(direccionPunto)
                .append(", codigoCliente=").append(codigoCliente)
                .append(", requestCliente=").append(requestCliente)
                .append(", requestOrder=").append(requestOrder)
                .append(", estado=").append(estado).append(", paso=")
                .append(paso).append(", nombreEjecutivo=")
                .append(nombreEjecutivo).append("]");
        return builder.toString();
    }

}
