package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase de transferencia para DirnumBlock.
 * 
 * @author TINet - Gustavo Arancibia V.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DirnumBlockTO implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 1L;

    /**
     * LowerExt.
     */
    @JsonProperty("lowerExt")
    private String lowerExt;

    /**
     * UpperExt.
     */
    @JsonProperty("upperExt")
    private String upperExt;

    /**
     * Permite obtener el valor del atributo lowerExt.
     * 
     * @return the lowerExt value.
     */
    public String getLowerExt() {
        return lowerExt;
    }

    /**
     * Permite establecer el valor del atributo lowerExt.
     *
     * @param lowerExt
     *            new value for lowerExt attribute.
     */
    public void setLowerExt(String lowerExt) {
        this.lowerExt = lowerExt;
    }

    /**
     * Permite obtener el valor del atributo upperExt.
     * 
     * @return the upperExt value.
     */
    public String getUpperExt() {
        return upperExt;
    }

    /**
     * Permite establecer el valor del atributo upperExt.
     *
     * @param upperExt
     *            new value for upperExt attribute.
     */
    public void setUpperExt(String upperExt) {
        this.upperExt = upperExt;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DirnumBlockTO [lowerExt=");
        builder.append(lowerExt);
        builder.append(", upperExt=");
        builder.append(upperExt);
        builder.append("]");
        return builder.toString();
    }

}
