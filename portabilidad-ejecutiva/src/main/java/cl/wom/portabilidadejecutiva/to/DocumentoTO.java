package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

/**
 * clase que representa los datos de un documento.
 *
 * @author TInet - Juan Pablo Vega.
 */
public class DocumentoTO  implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * atributo que representa la ruta del archivo.
     */
    private String ruta;

    /**
     * atributo que representa la extension del archivo.
     */
    private String ext;

    /**
     * atributo que representa el nombre del documento.
     */
    private String nombreDoc;

    /**
     * Permite obtener el valor del atributo ruta.
     * @return the ruta value.
     */
    public String getRuta() {
        return ruta;
    }

    /**
     * Permite establecer el valor del atributo ruta.
     *
     * @param ruta new value for ruta attribute.
     */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    /**
     * Permite obtener el valor del atributo ext.
     * @return the ext value.
     */
    public String getExt() {
        return ext;
    }

    /**
     * Permite establecer el valor del atributo ext.
     *
     * @param ext new value for ext attribute.
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * Permite obtener el valor del atributo nombreDoc.
     * @return the nombreDoc value.
     */
    public String getNombreDoc() {
        return nombreDoc;
    }

    /**
     * Permite establecer el valor del atributo nombreDoc.
     *
     * @param nombreDoc new value for nombreDoc attribute.
     */
    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DocumentoTO [ruta=").append(ruta).append(", ext=")
                .append(ext).append(", nombreDoc=").append(nombreDoc)
                .append("]");
        return builder.toString();
    }

}
