package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;
import java.sql.Date;

/**
 * 
 * TO para estado de portabilidades.
 *
 * @author Oscar Saavedra H.
 */
public class EstadoPortabilidadTO implements Serializable {

    /**
     * Serial.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Fecha.
     */
    private Date fecha;

    /**
     * Nombre ejecutivo.
     */
    private String nombreEjecutivo;

    /**
     * Estado.
     */
    private String estado;

    /**
     * Paso.
     */
    private String paso;

    /**
     * Permite obtener el valor del atributo fecha.
     * 
     * @return the fecha value.
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * Permite establecer el valor del atributo fecha.
     *
     * @param fecha
     *            new value for fecha attribute.
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * Permite obtener el valor del atributo nombreEjecutivo.
     * 
     * @return the nombreEjecutivo value.
     */
    public String getNombreEjecutivo() {
        return nombreEjecutivo;
    }

    /**
     * Permite establecer el valor del atributo nombreEjecutivo.
     *
     * @param nombreEjecutivo
     *            new value for nombreEjecutivo attribute.
     */
    public void setNombreEjecutivo(String nombreEjecutivo) {
        this.nombreEjecutivo = nombreEjecutivo;
    }

    /**
     * Permite obtener el valor del atributo estado.
     * 
     * @return the estado value.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Permite establecer el valor del atributo estado.
     *
     * @param estado
     *            new value for estado attribute.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Permite obtener el valor del atributo paso.
     * 
     * @return the paso value.
     */
    public String getPaso() {
        return paso;
    }

    /**
     * Permite establecer el valor del atributo paso.
     *
     * @param paso
     *            new value for paso attribute.
     */
    public void setPaso(String paso) {
        this.paso = paso;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EstadoPortabilidadTO [nombreEjecutivo=");
        builder.append(nombreEjecutivo);
        builder.append(", estado=");
        builder.append(estado);
        builder.append(", paso=");
        builder.append(paso);
        builder.append("]");
        return builder.toString();
    }

}
