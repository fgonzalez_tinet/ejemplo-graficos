package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * TO con los filtros de revisar estado portabilidad.
 *
 * @author Oscar Saavedra H.
 */
public class FiltroEstadoTO implements Serializable {

    /**
     * SerialVersionUID attribute.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Atributo fechaInicio.
     */
    private Date fechaInicio;

    /**
     * Atributo fechaFin.
     */
    private Date fechaFin;

    /**
     * Atributo ejecutivo del filtro ejecutivo.
     */
    private String ejecutivo;

    /**
     * Atributo nombrePunto.
     */
    private long idPunto;

    /**
     * Atributo idUsuario del usuario administrador logueado.
     */
    private String usuario;

    /**
     * Permite obtener el valor del atributo fechaInicio.
     * 
     * @return the fechaInicio value.
     */
    public Date getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Permite establecer el valor del atributo fechaInicio.
     *
     * @param fechaInicio
     *            new value for fechaInicio attribute.
     */
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * Permite obtener el valor del atributo fechaFin.
     * 
     * @return the fechaFin value.
     */
    public Date getFechaFin() {
        return fechaFin;
    }

    /**
     * Permite establecer el valor del atributo fechaFin.
     *
     * @param fechaFin
     *            new value for fechaFin attribute.
     */
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * Permite obtener el valor del atributo ejecutivo.
     * 
     * @return the ejecutivo value.
     */
    public String getEjecutivo() {
        return ejecutivo;
    }

    /**
     * Permite establecer el valor del atributo ejecutivo.
     *
     * @param ejecutivo
     *            new value for ejecutivo attribute.
     */
    public void setEjecutivo(String ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    /**
     * Permite obtener el valor del atributo idPunto.
     * 
     * @return the idPunto value.
     */
    public long getIdPunto() {
        return idPunto;
    }

    /**
     * Permite establecer el valor del atributo idPunto.
     *
     * @param idPunto
     *            new value for idPunto attribute.
     */
    public void setIdPunto(long idPunto) {
        this.idPunto = idPunto;
    }

    /**
     * Permite obtener el valor del atributo usuario.
     * 
     * @return the usuario value.
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Permite establecer el valor del atributo usuario.
     *
     * @param usuario
     *            new value for usuario attribute.
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FiltroEstadoTO [fechaInicio=");
        builder.append(fechaInicio);
        builder.append(", fechaFin=");
        builder.append(fechaFin);
        builder.append(", ejecutivo=");
        builder.append(ejecutivo);
        builder.append(", idPunto=");
        builder.append(idPunto);
        builder.append(", usuario=");
        builder.append(usuario);
        builder.append("]");
        return builder.toString();
    }

}
