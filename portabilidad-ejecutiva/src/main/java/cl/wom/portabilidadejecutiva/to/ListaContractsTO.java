package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase de transferencia para la lista Contracts.
 * 
 * @author TINet - Gustavo Arancibia V.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaContractsTO implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Lista de Contratos.
     */
    @JsonProperty("item")
    private List<ContractTO> item;

    /**
     * Permite obtener el valor del atributo item.
     * 
     * @return the item value.
     */
    public List<ContractTO> getItem() {
        return item;
    }

    /**
     * Permite establecer el valor del atributo item.
     *
     * @param item
     *            new value for item attribute.
     */
    public void setItem(List<ContractTO> item) {
        this.item = item;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ListaContractsTO [item=");
        builder.append(item);
        builder.append("]");
        return builder.toString();
    }

}
