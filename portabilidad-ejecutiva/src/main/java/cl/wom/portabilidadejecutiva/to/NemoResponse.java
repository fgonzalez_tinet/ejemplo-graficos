package cl.wom.portabilidadejecutiva.to;

public class NemoResponse {

	private String nombreNemo;

	private String detalleNemo;

	public String getNombreNemo() {
		return nombreNemo;
	}

	public void setNombreNemo(String nombreNemo) {
		this.nombreNemo = nombreNemo;
	}

	public String getDetalleNemo() {
		return detalleNemo;
	}

	public void setDetalleNemo(String detalleNemo) {
		this.detalleNemo = detalleNemo;
	}

}
