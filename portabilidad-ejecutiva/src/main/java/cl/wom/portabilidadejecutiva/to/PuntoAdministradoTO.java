package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

/**
 * 
 * Clase que refleja la relacion de un punto con un administrador.
 *
 * @author Oscar Saavedra H.
 */
public class PuntoAdministradoTO implements Serializable {

    /**
     * SerialVersionUID attribute.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Atributo Id del punto.
     */
    private Long id;

    /**
     * Atributo Nombre Corto.
     */
    private String nombreCorto;

    /**
     * Atributo Nombre Administrador del punto.
     */
    private String nombreAdministrador;

    /**
     * Permite obtener el valor del atributo id.
     * 
     * @return the id value.
     */
    public Long getId() {
        return id;
    }

    /**
     * Permite establecer el valor del atributo id.
     *
     * @param id
     *            new value for id attribute.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Permite obtener el valor del atributo nombreCorto.
     * 
     * @return the nombreCorto value.
     */
    public String getNombreCorto() {
        return nombreCorto;
    }

    /**
     * Permite establecer el valor del atributo nombreCorto.
     *
     * @param nombreCorto
     *            new value for nombreCorto attribute.
     */
    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    /**
     * Permite obtener el valor del atributo nombreAdministrador.
     * 
     * @return the nombreAdministrador value.
     */
    public String getNombreAdministrador() {
        return nombreAdministrador;
    }

    /**
     * Permite establecer el valor del atributo nombreAdministrador.
     *
     * @param nombreAdministrador
     *            new value for nombreAdministrador attribute.
     */
    public void setNombreAdministrador(String nombreAdministrador) {
        this.nombreAdministrador = nombreAdministrador;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PuntoAdministradoTO punto = (PuntoAdministradoTO) obj;
        if (punto.id == null || id == null) {
            return false;
        }
        return ((PuntoAdministradoTO) obj).id.equals(id);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PuntoAdministrado [id=");
        builder.append(id);
        builder.append(", nombreCorto=");
        builder.append(nombreCorto);
        builder.append(", nombreAdministrador=");
        builder.append(nombreAdministrador);
        builder.append("]");
        return builder.toString();
    }

}
