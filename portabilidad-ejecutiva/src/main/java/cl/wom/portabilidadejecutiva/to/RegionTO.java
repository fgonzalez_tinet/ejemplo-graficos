package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

/**
 * Clase que contiene los datos de las regiones.
 *
 * @author TInet - Juan Pablo Vega
 */
public class RegionTO implements Serializable  {

    private static final long serialVersionUID = 1L;

    /**
     * atributo que representa el codigo de la operadora.
     */
    private String codigo;

    /**
     * atributo que representa el nombre de la operadora.
     */
    private String nombre;

    /**
     * Permite obtener el valor del atributo codigo.
     * 
     * @return the codigo value.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Permite establecer el valor del atributo codigo.
     *
     * @param codigo
     *            new value for codigo attribute.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Permite obtener el valor del atributo nombre.
     * 
     * @return the nombre value.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Permite establecer el valor del atributo nombre.
     *
     * @param nombre
     *            new value for nombre attribute.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OperadoresTO [codigo=").append(codigo)
                .append(", nombre=").append(nombre).append("]");
        return builder.toString();
    }

}
