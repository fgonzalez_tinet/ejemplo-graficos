package cl.wom.portabilidadejecutiva.to;

import java.io.Serializable;

/**
 * Clase que representa al Usuario.
 * 
 * @author TInet - Esteban Pavez A.
 */
public class UsuarioTO implements Serializable {

    /**
     * Atributo serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Atributo Nombre de usuario.
     */
    private String nombreUsuario;

    /**
     * Atributo Nombre.
     */
    private String nombre;

    /**
     * Atributo Apellidos.
     */
    private String apellidoPaterno;

    /**
     * Atributo Password.
     */
    private String password;

    /**
     * Atributo Perfil.
     */
    private Integer perfil;

    /**
     * Atributo Punto.
     */
    private Long punto;

    /**
     * 
     * Permite obtener el valor del atributo nombreUsuario.
     *
     * @return valor de nombreUsuario.
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    /**
     * 
     * Permite establecer el valor del atributo nombreUsuario.
     *
     * @param nombreUsuario
     *            nuevo valor para el atributo nombreUsuario.
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    /**
     * 
     * Permite obtener el valor del atributo nombre.
     *
     * @return valor de nombre.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * Permite establecer el valor del atributo nombre.
     *
     * @param nombre
     *            nuevo valor para el atributo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * 
     * Permite obtener el valor del atributo apellidoPaterno.
     *
     * @return valor de apellido paterno.
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * 
     * Permite establecer el valor del atributo apellidoPaterno.
     *
     * @param apellidoPaterno
     *            nuevo valor para el atributo apellidoPaterno.
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * 
     * Permite obtener el valor del atributo perfil.
     *
     * @return valor de perfil.
     */
    public Integer getPerfil() {
        return perfil;
    }

    /**
     * 
     * Permite establecer el valor del atributo perfil.
     *
     * @param perfil
     *            nuevo valor para el atributo perfil.
     */
    public void setPerfil(Integer perfil) {
        this.perfil = perfil;
    }

    /**
     * 
     * Permite obtener el valor del atributo punto.
     *
     * @return valor de punto.
     */
    public Long getPunto() {
        return punto;
    }

    /**
     * 
     * Permite establecer el valor del atributo punto.
     *
     * @param punto
     *            nuevo valor para el atributo punto.
     */
    public void setPunto(Long punto) {
        this.punto = punto;
    }

    /**
     * 
     * Permite obtener el valor del atributo password.
     *
     * @return valor de password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * Permite establecer el valor del atributo password.
     *
     * @param password
     *            nuevo valor para el atributo password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UsuarioTO [nombreUsuario=");
        builder.append(nombreUsuario);
        builder.append(", nombre=");
        builder.append(nombre);
        builder.append(", apellidoPaterno=");
        builder.append(apellidoPaterno);
        builder.append(", password=");
        builder.append(password);
        builder.append(", perfil=");
        builder.append(perfil);
        builder.append(", punto=");
        builder.append(punto);
        builder.append("]");
        return builder.toString();
    }

}
