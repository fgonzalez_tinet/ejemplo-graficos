package cl.wom.portabilidadejecutiva.util;

import org.springframework.http.HttpHeaders;

/**
 * Utility class for HTTP headers creation.
 *
 * @author Kendru Estrada.
 */
public final class HeaderUtil {

    /**
     * 
     * HeaderUtil constructor.
     *
     */
    private HeaderUtil() {
    }

    /**
     * 
     * Metodo createAlert.
     *
     * @param message mensaje.
     * @param param parametros.
     * @return Cabecera.
     */
    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-portabilidadEjecutivaApp-alert", message);
        headers.add("X-portabilidadEjecutivaApp-params", param);
        return headers;
    }

    /**
     * 
     * Metodo createEntityCreationAlert.
     *
     * @param entityName nombre de la entidad.
     * @param param parametro.
     * @return Cabecera.
     */
    public static HttpHeaders createEntityCreationAlert(String entityName,
            String param) {
        return createAlert(
                "A new " + entityName + " is created with identifier " + param,
                param);
    }

    /**
     * 
     * Metodo createEntityUpdateAlert.
     *
     * @param entityName nombre de la entidad.
     * @param param parametros.
     * @return cabecera.
     */
    public static HttpHeaders createEntityUpdateAlert(String entityName,
            String param) {
        return createAlert(
                "A " + entityName + " is updated with identifier " + param,
                param);
    }

    /**
     * 
     * Metodo createEntityDeletionAlert.
     *
     * @param entityName nombre de la entidad.
     * @param param parametros.
     * @return Cabecera.
     */
    public static HttpHeaders createEntityDeletionAlert(String entityName,
            String param) {
        return createAlert(
                "A " + entityName + " is deleted with identifier " + param,
                param);
    }

    /**
     * 
     * Metodo createFailureAlert.
     *
     * @param entityName nombre de la entidad.
     * @param errorKey llave de error.
     * @param defaultMessage mensaje por defecto.
     * @return Cabecera.
     */
    public static HttpHeaders createFailureAlert(String entityName,
            String errorKey, String defaultMessage) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-portabilidadEjecutivaApp-errorkey", errorKey);
        headers.add("X-portabilidadEjecutivaApp-error", defaultMessage);
        headers.add("X-portabilidadEjecutivaApp-params", entityName);
        return headers;
    }
}
