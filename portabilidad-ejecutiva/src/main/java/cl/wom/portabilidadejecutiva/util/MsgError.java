package cl.wom.portabilidadejecutiva.util;

/**
 * Clase con las key para los distintos mensajes de error de la aplicacion.
 *
 * @author TInet - Juan Pablo Vega.
 */
public enum MsgError {

    /**
     * Mensaje de error cuando la url del mensaje esta mal formada.
     */
    ERROR_WS_URL("portabilidad.ws.error.url"),

    /**
     * Mensaje de error cuando la respuesta del servicio es nula.
     */
    ERROR_WS_RESPUESTA("portabilidad.ws.error.respuesta"),

    /**
     * Mensaje de error cuando existe un problema en la invocacion del
     * servicio.
     */
    ERROR_WS_LLAMADA("portabilidad.ws.error.llamada"),

    /**
     * Mensaje de error cuando los parametros de entrada al servicio estan
     * incorrectos.
     */
    ERROR_WS_PARAMETROS("portabilidad.ws.error.parametrosIn"),

    /**
     * Mensaje de error cuando el servicio de validacion de chip wom no
     * encuentra un contrato asociado.
     */
    ERROR_CHIP_CONTRATO("portabilidad.chip.error.contrato"),

    /**
     * Mensaje de error cuando el servicio de validacion de chip wom no
     * encuentra el numero asociado.
     */
    ERROR_CHIP_NUMERO("portabilidad.chip.error.numero"),

    /**
     * Mensaje de error general del sistema.
     */
    ERROR_GENERAL("portabilidad.error.general"),

    /**
     * Mensjae de error cuando el chip no esta provisionado
     * para la portabilidad.
     */
    ERROR_CHIP_PLAN("portabilidad.chip.error.plan"),

    /**
     * Mensaje de error cuando el chip no es valido.
     */
    ERROR_CHIP_VALIDEZ("portabilidad.chip.error.validez"),

    /**
     * mensaje de error cuando no se pudo contactar a la compañia de origen.
     */
    ERROR_TELEFONO_CIA("portabilidad.telefono.error.cia"),

    /**
     * mensaje de error cuando el telefono a portar esta desactivado.
     */
    ERROR_TELEFONO_DESACTIVADO("portabilidad.telefono.error.desactivado"),

    /**
     * mensaje de error cuando el numero de origen corresponde a postpago.
     */
    ERROR_TELEFONO_POSTPAGO("portabilidad.telefono.error.postpago"),

    /**
     * mensaje de error cuando el numero a portar tiene deudas.
     */
    ERROR_TELEFONO_DEUDA("portabilidad.telefono.error.deuda"),

    /**
     * mensaje de error cuando el telefono se encuentra el permanencia.
     */
    ERROR_TELEFONO_PERMANENCIA("portabilidad.telefono.error.permanencia"),

    /**
     * mensaje de error cuando el telefono no corresponde a la compañia de
     * origen seleccionada.
     */
    ERROR_TELEFONO_ORIGEN("portabilidad.telefono.error.origen"),

    /**
     * mensaje de error cuando cliente no posee la antiguedad para portarse.
     */
    ERROR_IMEI_ANTIGUEDAD("portabilidad.imei.error.antiguedad"),

    /**
     * mensaje de error cuando el imei esta reportado por robo.
     */
    ERROR_IMEI_ROBADO("portabilidad.imei.error.robado"),

    /**
     * mensaje de error cuando el imei ya se encuentra
     * en proceso de portabilidad.
     */
    ERROR_IMEI_PORTABILIDAD("portabilidad.imei.error.portabilidad"),

    /**
     * mensaje de error cuando el servicio de siebel no responde datos para el
     * cliente.
     */
    ERROR_CLIENTE_SIEBEL("portabilidad.cliente.siebel"),

    /**
     * mensaje de error cuando no se fue posible activar la cuenta de
     * facturacion para un cliente existente.
     */
    ERROR_FACTURACION_REACTIVAR("portabilidad.facturacion.error");

    /**
     * Atributo para obtener el texto del enum.
     */
    private String value;

    /**
     * Contructor de la clase.
     *
     * @param value enum.
     */
    MsgError(String value) {
        this.value = value;
    }

    /**
     * obtiene el valor del enum.
     *
     * @return valor.
     */
    public String getValue() {
        return this.value;
    }

}
