package cl.wom.portabilidadejecutiva.util;

/**
 * Clase utilitaria que contiene mensajes genericos utilizados en toda la
 * aplicacion.
 *
 * @author TInet - Juan Pablo Vega
 */
public final class PortabilidadEjecutivaConstant {

    /**
     * valor por defecto del recepientId para invocar a los servicios web.
     */
    public static final String DEFAULT_RECEPIENT = "255";

    /**
     * valor por defecto del codigo cap en el caso que no se haya ingresado.
     */
    public static final String DEFAULT_CAP = "0";

    /**
     * llave para obtener el valor del tipo de servicio desde las propiedad de
     * base de datos.
     */
    public static final String KEY_SERVICIO_NPG = "npg.serviceType";

    /**
     * llave para obtener la url del ws desde la base de datos.
     */
    public static final String KEY_ENDPOINT_NPG = "endpoint.npg";

    /**
     * en el caso que la validacion de la respuesta del ws sea satisfactoria.
     */
    public static final String MENSAJE_OK = "OK";

    /**
     * mensaje cuando cliente no existe.
     */
    public static final String CLIENTE_NO_EXISTE = "CLIENTE NO EXISTE";

    /**
     * codigo retornado por los servicios cuando los parametros enviados no
     * son correctos.
     */
    public static final String CODIGO_ERROR_RESPUESTA_WS = "0003";

    /**
     * Nombre del servicio NPG.
     */
    public static final String SERVICIO_NPG = "BIPInterfaceService";

    /**
     * nombre del namespace del servicio npg.
     */
    public static final String NAMESPACE_NPG =
            "http://interfaces.publish.ws.bip.npg.adexus.cl/";

    /**
     * nombre del namespace del servicio de siebel.
     */
    public static final String NAMESPACE_SIEBEL = "http://nii.com/Enterprise"
            + "ProxyService/AccountManagement/QueryCustomerListDataWSDL/V1";

    /**
     * nombre del servicio de siebel.
     */
    public static final String SERVICIO_SIEBEL = "NII_IVR_WS";

    /**
     * llave para obtener la url del ws desde la base de datos.
     */
    public static final String KEY_ENDPOINT_SIEBEL = "endpoint.siebel";

    /**
     * llave del parametro planes de prepago.
     */
    public static final String KEY_PLANES_PREPAGO =
            "planesprepago.portabilidad";

    /**
     * nombre del servicio de contrato.
     */
    public static final String SERVICIO_CONTRATO = "ContractsSearchService";

    /**
     * nombre del namespace del servicio de contrato.
     */
    public static final String NAMESPACE_CONTRATO =
            "http://www.lhsgroup.com/ws_v2_NII_1";

    /**
     * llave para obtener la key utilizada en el header del request del
     * servicio de contrato.
     */
    public static final String KEY_CONTRACTS = "contractSearch.data.key";

    /**
     * llave para obtener el value utilizado en el header del request del
     * servicio de contrato.
     */
    public static final String VALUE_CONTRACTS = "contractSearch.data.value";

    /**
     * llave para obtener la url del ws desde la base de datos.
     */
    public static final String KEY_ENDPOINT_CONTRACT =
            "endpoint.contractsSearch";

    /**
     * llave para obtener la url del ws desde base de datos.
     */
    public static final String KEY_ENDPOINT_FACTURA = "endpoint.creaclientefac";

    /**
     * namespace del servicio de factura.
     */
    public static final String NAMESPACE_FACTURA = "http://siebel.com/wom/";

    /**
     * nombre del servicio de factura.
     */
    public static final String SERVICIO_FACTURA =
            "NII_spcPIV_spcCustomer_spcFlat_spcRUT_spcBSQSService";

    /**
     * tamaño de rut.
     */
    public static final int LARGO_RUT = 9;

    /**
     * largo 8 del telefono.
     */
    public static final int LARGO_TEL_8 = 8;

    /**
     * largo 8 del telefono.
     */
    public static final int LARGO_TEL_9 = 9;

    /**
     * DIEZ.
     */
    public static final int DIEZ = 10;

    /**
     * NUEVE.
     */
    public static final int NUEVE = 9;

    /**
     * ONCE.
     */
    public static final int ONCE = 11;

    /**
     * SEIS.
     */
    public static final int SEIS = 6;

    /**
     * CUARENTA_SIETE.
     */
    public static final int CUARENTA_SIETE = 47;

    /**
     * SETENTA_CINCO.
     */
    public static final int SETENTA_CINCO = 75;

    /**
     * LLAVE OPERADORAS.
     */
    public static final String KEY_OPERADORA = "operadoras.part.uno";

    /**
     * Llave para consultar regiones.
     */
    public static final String KEY_REGION = "REG";

    /**
     * PASO 2.
     */
    public static final int PASO_2 = 2;

    /**
     * PASO 3.
     */
    public static final int PASO_3 = 3;

    /**
     * ESTADO_EN_CURSO.
     */
    public static final int ESTADO_EN_CURSO = 1;

    /**
     * ESTADO_COMPLETADO.
     */
    public static final int ESTADO_COMPLETADO = 2;

    /**
     * ESTADO_CON_PROBLEMAS.
     */
    public static final int ESTADO_CON_PROBLEMAS = 3;

    /**
     * ESTADO_CANCELADO.
     */
    public static final int ESTADO_CANCELADO = 4;

    /**
     * the priority for this message.
     */
    public static final int JMS_QUEUE_PRIORITY = 9;

    /**
     * the message's lifetime (in milliseconds).
     */
    public static final long JMS_QUEUE_TIMETOLIVE = 0;

    /**
     * SEGUNDOS.
     */
    public static final int SEGUNDOS = 10000;

    /**
     * TIEMPO_FIJO.
     */
    public static final int TIEMPO_FIJO = 10000;

    /**
     * TIEMPO_MAXIMO.
     */
    public static final int TIEMPO_MAXIMO = 180000;

    /**
     * llave para obtener la url del ws desde base de datos.
     */
    public static final String KEY_ENDPOINT_PDF = "endpoint.generatePdf";

    /**
     * namespace del servicio de factura.
     */
    public static final String NAMESPACE_PDF =
            "http://www.nextel.cl/mwGeneratePDFDocument/1.0";

    /**
     * nombre del servicio de factura.
     */
    public static final String SERVICIO_PDF = "mwGeneratePDFDocument";

    /**
     * descripcion del tipo de documento pdf.
     */
    public static final String CEDULA_IDENTIDAD_SOLIC =
            "Cedula de Identidad Solicitante";

    /**
     * descripcion del tipo de documento pdf.
     */
    public static final String CONTRATO_PORTABILIDAD =
            "Contrato de Portabilidad";

    /**
     * llave para obtener la url del ws desde base de datos.
     */
    public static final String KEY_ENDPOINT_INVENTARIO =
            "endpoint.confirma-rebaja-inventario";

    /**
     * namespace del servicio de factura.
     */
    public static final String NAMESPACE_INVENTARIO =
            "http://www.nextel.cl/mwInventoryReductionConfirmation/1.0";

    /**
     * nombre del servicio de factura.
     */
    public static final String SERVICIO_INVENTARIO =
            "mwInventoryReductionConfirmation";

    /**
     * estado del cliente en seibel.
     */
    public static final String CLIENTE_PROSPECTO = "Prospect";

    /**
     * estado del cliente en siebel.
     */
    public static final String CLIENTE_INACTIVO = "Inactive";

    /**
     * Llave para obtener el valor del tipo de servicio rest para
     * ownershipandAccountValidation desde las propiedad de base de datos.
     */
    public static final String KEY_SERVICIO_REST_NPG_OWNER =
            "endpoint.rest.npg.ownershipandAccountValidation";

    /**
     * Llave para obtener el valor del tipo de servicio rest para
     * equipmentNumberValidation desde las propiedad de base de datos.
     */
    public static final String KEY_SERVICIO_REST_NPG_EQUIPMENT =
            "endpoint.rest.npg.equipmentNumberValidation";

    /**
     * Llave para obtener el valor del tipo de servicio rest para
     * CAPGeneration desde las propiedad de base de datos.
     */
    public static final String KEY_SERVICIO_REST_NPG_CAP =
            "endpoint.rest.npg.capGeneration";

    /**
     * Llave para obtener el valor del tipo de servicio rest para
     * getCustomerInformation desde las propiedad de base de datos.
     */
    public static final String KEY_SERVICIO_REST_SIEBEL_CUSTOMER =
            "endpoint.rest.siebel.getCustomerInformation";

    /**
     * Llave para obtener el valor del tipo de servicio rest para execute
     * desde las propiedad de base de datos.
     */
    public static final String KEY_SERVICIO_REST_CONTRACT_EXECUTE =
            "endpoint.rest.contractsSearch.execute";

    /**
     * Llave para obtener el valor del endpoint correspondiente al Servicio
     * Proxy REST PSLdapAgregarREST.
     */
    public static final String KEY_ENDPOINT_REST_LDAP_AGREGAR =
            "endpoint.rest.ldap.agregar";

    /**
     * Llave para obtener el valor del endpoint correspondiente al Servicio
     * Proxy REST PSLdapEditarREST.
     */
    public static final String KEY_ENDPOINT_REST_LDAP_EDITAR =
            "endpoint.rest.ldap.editar";

    /**
     * Llave para obtener el valor del endpoint correspondiente al Servicio
     * Proxy REST PSLdapEliminarREST.
     */
    public static final String KEY_ENDPOINT_REST_LDAP_ELIMINAR =
            "endpoint.rest.ldap.eliminar";

    /**
     * Llave para obtener el valor del endpoint correspondiente al Servicio
     * Proxy REST PSLdapBuscarREST.
     */
    public static final String KEY_ENDPOINT_REST_LDAP_BUSCAR =
            "endpoint.rest.ldap.buscar";

    /**
     * Llave para obtener el valor del endpoint correspondiente al Servicio
     * Proxy REST PSLdapEditarContraseniaREST.
     */
    public static final String KEY_ENDPOINT_REST_LDAP_EDITAR_CONTRASENIA =
            "endpoint.rest.ldap.editarContrasenia";

    /**
     * Llave para obtener el valor del endpoint correspondiente al Servicio
     * Proxy REST PSLdapEditarGrupoREST.
     */
    public static final String KEY_ENDPOINT_REST_LDAP_EDITAR_GRUPO =
            "endpoint.rest.ldap.editarGrupo";

    /**
     * Llave para obtener el valor del DN (Nombre Distinguido) en el que son
     * almacenados los usuarios en LDAP.
     */
    public static final String KEY_LDAP_DN_USUARIOS = "ldap.dn.usuarios";

    /**
     * Llave para obtener el valor del DN (Nombre Distinguido) en el que son
     * almacenados los grupos en LDAP.
     */
    public static final String KEY_LDAP_DN_GRUPOS = "ldap.dn.grupos";

    /**
     * LLave para obtener el valor del grupo Super Admin de LDAP.
     */
    public static final String KEY_LDAP_GRUPO_SUPER_ADMIN =
            "ldap.grupo.superAdmin";

    /**
     * LLave para obtener el valor del grupo Admin Promo de LDAP.
     */
    public static final String KEY_LDAP_GRUPO_ADMIN_PROMO =
            "ldap.grupo.adminPromo";

    /**
     * LLave para obtener el valor del grupo Promotores de LDAP.
     */
    public static final String KEY_LDAP_GRUPO_PROMOTORES =
            "ldap.grupo.promotores";

    /**
     * Llave para obtener el valor del objeto que almacena al usuario en LDAP.
     */
    public static final String KEY_LDAP_USUARIO_OBJETO = "ldap.usuario.objeto";

    /**
     * Llave para obtener el valor del objeto que almacena al grupo en LDAP.
     */
    public static final String KEY_LDAP_GRUPO_OBJETO = "ldap.grupo.objeto";

    /**
     * Nombre del método a usar para agregar.
     */
    public static final String METODO_AGREGAR = "add";

    /**
     * Nombre del método a usar para editar.
     */
    public static final String METODO_EDITAR = "replace";

    /**
     * Nombre del método a usar para eliminar.
     */
    public static final String METODO_ELIMINAR = "delete";

    /**
     * Llave del filtro usado para buscar todos los usuarios de LDAP.
     */
    public static final String KEY_LDAP_FILTRO_BUSCAR_TODOS =
            "ldap.filtro.buscarTodos";

    /**
     * Constructor por omision.
     */
    private PortabilidadEjecutivaConstant() {
    }
}
