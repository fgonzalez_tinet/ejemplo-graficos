package cl.wom.portabilidadejecutiva.util;

/**
 * Define los roles que se utilizan en la aplicacion. Este listado hace
 * referencia los roles definidos en los archivos de configuraci&oacute;n.
 * <li><b><i>web.xml</i></b> en la secci&oacute;n <i>security-role</i>.
 * </li>
 * <li><b><i>weblogic.xml</i></b> en la seccion
 * <i>security-role-assignment</i> en donde <i>role-name</i> corresponde
 * al nombre de rol definido en el archivo web.xml y <i>principal-name</i>
 * corresponde al rol definido en el administrador de permisos (LDAP) en
 * el servidor de aplicaciones (Weblogic).
 * 
 * @author Francisco Mendoza Cabrera.
 *
 */
public enum RolesEnum {

    /**
     * Rol de adminsitrador.
     */
    SUPERADMIN(1),

    /**
     * Rol de usuario de backoffice.
     */
    ADMINISTRADOR(2),

    /**
     * Rol de usuario ejecutivo.
     */
    EJECUTIVO(3);

    /**
     * Codigo identificador del rol.
     */
    private final int codigo;

    /**
     * Glosa asociada al rol en el servidor.
     */
    private final String glosa;

    /**
     * Constructor de la clase.
     * 
     * @param codigo
     *            Codigo identificador del rol.
     */
    RolesEnum(int codigo) {
        this.codigo = codigo;
        this.glosa = this.name().toLowerCase();
    }

    /**
     * Constructor de la clase.
     * 
     * @param codigo
     *            Codigo identificador del rol.
     * @param glosa
     *            Numero identificador del rol.
     */
    RolesEnum(int codigo, String glosa) {
        this.codigo = codigo;
        this.glosa = glosa;
    }

    /**
     * Obtiene el codigo numerico identificador del rol seleccionado.
     * 
     * @return Codigo del rol.
     */
    public int obtenerCodigo() {
        return this.codigo;
    }

    /**
     * Obtiene el la descripcion del rol seleccionado.
     * 
     * @return Obtiene la descripcion del rol seleccionado.
     */
    public String obtenerGlosa() {
        return this.glosa;
    }
}
