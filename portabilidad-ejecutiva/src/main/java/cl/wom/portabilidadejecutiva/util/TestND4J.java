package cl.wom.portabilidadejecutiva.util;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class TestND4J {

	public static void main(String[] args) {

		INDArray arr1 = Nd4j.create(new float[] { 1, 2, 3, 4 }, new int[] { 2, 2 });

		System.out.println("*********************************ARREGLO 1*********************************");
		System.out.println(arr1);

		System.out.println("*********************************SUMAR 1***********************************");
		arr1.addi(1);
		System.out.println(arr1);

		System.out.println("*********************************ARREGLO 2*********************************");
		INDArray arr2 = Nd4j.create(new float[] { 5, 6, 7, 8 }, new int[] { 2, 2 });
		System.out.println(arr2);
		System.out.println("*********************************ARREGLO 1 + 2*****************************");
		arr1.addi(arr2);
		System.out.println(arr1);

		System.out.println("*********************************EJEMPLO 1*********************************");
		INDArray nd = Nd4j.create(new float[] { 1, 2, 3, 4 }, new int[] { 2, 2 });
		System.out.println(nd);
		System.out.println("*********************************EJEMPLO 1 ADD*****************************");
		nd.addi(1);
		System.out.println(nd);
		System.out.println("*********************************EJEMPLO 1 MUL*****************************");
		nd.muli(5);
		System.out.println(nd);
		System.out.println("*********************************EJEMPLO 1 RESTA***************************");
		nd.subi(5);
		System.out.println(nd);
		System.out.println("*********************************EJEMPLO 1 DIV*****************************");
		nd.divi(5);
		System.out.println(nd);

		System.out.println("*********************************EJEMPLO VECTORES**************************");
		INDArray nd1 = Nd4j.create(new float[] { 1, 2, 3, 4 }, new int[] { 2, 2 });
		System.out.println(nd1);
		System.out.println("*********************************VECTOR 2**********************************");
		INDArray nd2 = Nd4j.create(new float[] { 5, 6 }, new int[] { 2, 1 }); // vector as column
		System.out.println(nd2);
		System.out.println("*********************************VECTOR SUMA**********************************");
		nd1.addiColumnVector(nd2);
		System.out.println(nd1);
		
		System.out.println("*********************************VECTOR 3**********************************");
		INDArray nd3 = Nd4j.create(new float[]{5,6},new int[]{1, 2}); //vector as row
		System.out.println(nd3);
		nd1.addiRowVector(nd3);
		System.out.println(nd1);
	}

}
