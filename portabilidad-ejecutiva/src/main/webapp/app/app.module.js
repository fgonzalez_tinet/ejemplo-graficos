(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp', [ 'ngResource', 'ui.bootstrap',
			'ui.router', 'pascalprecht.translate', 'ngMaterial' ]);

})();
