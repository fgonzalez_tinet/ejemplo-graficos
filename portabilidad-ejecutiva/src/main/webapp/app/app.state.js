(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').config(stateConfig);

	stateConfig.$inject = [ '$urlRouterProvider', '$stateProvider',
			'$httpProvider' ];

	function stateConfig($urlRouterProvider, $stateProvider, $httpProvider) {

		// se agrega esta cabecera a todas las solicitudes http
		// para que el browser no solicite el usuario y password.
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

		$urlRouterProvider.otherwise("menu");
		$stateProvider.state('app', {
			abstract : true
		});
	}
})();
