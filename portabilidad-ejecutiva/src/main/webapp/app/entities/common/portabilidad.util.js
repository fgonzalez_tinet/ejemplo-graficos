var app = angular.module('portabilidadEjecutivaApp');

app.directive('soloNumeros', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				if (text) {
					var transformedInput = text.replace(/[^0-9-]/g, '');
					if (transformedInput !== text) {
						ngModelCtrl.$setViewValue(transformedInput);
						ngModelCtrl.$render();
					}
					return transformedInput;
				}
				return undefined;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

app.directive('soloRut', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				if (text) {
					var transformedInput = text.replace(/[^0-9kK_]/g, '');
					if (transformedInput !== text) {
						ngModelCtrl.$setViewValue(transformedInput);
						ngModelCtrl.$render();
					}
					return transformedInput;
				}
				return undefined;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

app.directive('soloAlpha', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attr, ngModelCtrl) {
			function fromUser(text) {
				if (text) {
					var transformedInput = text.replace(/[^0-9A-Za-z_ ]/g, '');
					if (transformedInput !== text) {
						ngModelCtrl.$setViewValue(transformedInput);
						ngModelCtrl.$render();
					}
					return transformedInput;
				}
				return undefined;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

app.directive("limitTo", [ function() {
	return {
		restrict : "A",
		link : function(scope, elem, attrs) {
			var limit = parseInt(attrs.limitTo);
			angular.element(elem).on("keypress", function(e) {
				if (this.value.length == limit)
					e.preventDefault();
			});
		}
	}
} ]);

app.directive('soloUsuario', function() {
	return {
		require : 'ngModel',
		link : function(scope, elem, attrs) {
			angular.element(elem).on("keypress", function(e) {
				key = e.keyCode || e.which;
			    tecla = String.fromCharCode(key).toLowerCase();
				letras = "abcdefghijklmnñopqrstuvwxyz0123456789_";
				if (letras.indexOf(tecla) == -1) {
					return false;
				} else {
					return tecla;
				}
			});
		}
	};
});

app.directive('soloMail', function() {
	return {
		require : 'ngModel',
		link : function(scope, elem, attrs) {
			angular.element(elem).on("keypress", function(e) {
				key = e.keyCode || e.which;
			    tecla = String.fromCharCode(key).toLowerCase();
				letras = "abcdefghijklmnñopqrstuvwxyz0123456789_.@";
				if (letras.indexOf(tecla) == -1) {
					return false;
				} else {
					return tecla;
				}
			});
		}
	};
});

app.directive('soloSerie', function() {
	return {
		require : 'ngModel',
		link : function(scope, elem, attrs) {
			angular.element(elem).on("keypress", function(e) {
				key = e.keyCode || e.which;
			    tecla = String.fromCharCode(key).toLowerCase();
				letras = "a0123456789";
				if (letras.indexOf(tecla) == -1) {
					return false;
				} else {
					return tecla;
				}
			});
		}
	};
});
