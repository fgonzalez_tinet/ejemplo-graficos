(function() {
    'use strict';

    angular
        .module('portabilidadEjecutivaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('entity', {
            parent: 'app'
        })
        .state('principal', {
            url: '/principal',
            parent: 'app',
            templateUrl: 'app/entities/menu/menu.html',
            controller: 'MenuController'
        });
    }
   
})();
