(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller(
			'EstadoPortabilidadController', EstadoPortabilidadController);

	EstadoPortabilidadController.$inject = [ '$rootScope', '$scope', '$state',
			'Punto', 'Estado', 'Usuario' ];

	function EstadoPortabilidadController($rootScope, $scope, $state, Punto,
			Estado, Usuario) {
		var epc = this;
		$scope.filtros = {};
		$scope.filtros.fechaInicio = new Date();
		$scope.filtros.fechaFin = new Date();

		$scope.onClick = true;
		$scope.puntos = [];
		$scope.usuarios = [];
		$scope.estadosPortabilidad = [];

		$scope.filtros.usuario = $rootScope.usuario.nombre;

		$scope.listarPuntos = function() {
			Punto.query(function(result) {
				$scope.puntos = result;
			})
		};

		$scope.listarUsuarios = function() {
			Usuario.query(function(result) {
				$scope.usuarios = result;
			})
		};

		$scope.validarFiltros = function() {

			$scope.filtros.ejecutivo = '';

			if ($rootScope.usuario.perfil === 2) {
				$scope.filtros.idPunto = $rootScope.usuario.punto;
				document.getElementById('idPunto').disabled = true;
			}

			if ($rootScope.usuario.perfil === 3) {
				$scope.filtros.idPunto = $rootScope.usuario.punto;
				$scope.filtros.ejecutivo = $rootScope.usuario.nombre;
				document.getElementById('idPunto').disabled = true;
				document.getElementById('idEjecutivo').disabled = true;
			}
		};

		epc.buscar = function() {
			showLoading();

			if ($scope.filtros.ejecutivo === '') {
				$scope.filtros.ejecutivo = 'todos';
			}

			Estado.query($scope.filtros, function(result) {
				$scope.estadosPortabilidad = result;
				$scope.onClick = false;
				hideLoading();
			}, function(result) {
				hideLoading();
			})
		};

		$scope.listarPuntos();
		$scope.listarUsuarios();
		$scope.validarFiltros();
	}
})();
