(function() {
	'use strict';
	angular.module('portabilidadEjecutivaApp').factory('Estado', Punto);

	Punto.$inject = [ '$resource' ];

	function Punto($resource) {
		var resourceUrl = 'api/estados';

		return $resource(resourceUrl, {}, {
			'query' : { method : 'POST', transformResponse : function(data) {
					data = angular.fromJson(data);
					return data;
					}, 
					isArray: true
					}	
		});
	}
})();
