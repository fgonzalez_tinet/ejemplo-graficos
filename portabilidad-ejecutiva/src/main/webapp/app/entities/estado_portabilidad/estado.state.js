(function() {
    'use strict';

    angular
        .module('portabilidadEjecutivaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('estado', {
            parent: 'entity',
            url: '/estado',
            data: {
                pageTitle: 'Estado Portabilidad'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/estado_portabilidad/estado.html',
                    controller: 'EstadoPortabilidadController',
                    controllerAs: 'epc'
                }
            }
        });
    }

})();
