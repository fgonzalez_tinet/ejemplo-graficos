(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller('GraficoController',
			GraficoController);

	GraficoController.$inject = [ '$scope', 'Grafico' ];

	function GraficoController($scope, Grafico) {
		var gc = this;
		$(function() {
			$.getJSON('http://localhost:8080/acciones/nemosporid/1',
			function(data) {
				// Create the chart
				$('#container_grafico').highcharts('StockChart', {
					
					rangeSelector : {
						selected : 1
					},

					title : {
						text : data.nombreNemo
					},

					series : [ {
						name : data.nombreNemo,
						data : JSON.parse(data.detalleNemo),
						tooltip : {
							valueDecimals : 2
						}
					} ]
					
				});
			});
		});
	}
	
})();
