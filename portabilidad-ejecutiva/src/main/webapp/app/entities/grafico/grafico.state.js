(function() {
    'use strict';

    angular.module('portabilidadEjecutivaApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('grafico', {
            parent: 'entity',
            url: '/grafico',
            data: {
                pageTitle: 'Graficos'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/grafico/grafico.html',
                    controller: 'GraficoController',
                    controllerAs: 'gc'
                }
            }
        })
    }

})();
