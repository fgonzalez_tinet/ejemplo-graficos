(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller('GraficoMultController',
			GraficoMultController);
	
	var seriesOptions = [];
	var seriesCounter = 0;
	var graficos = [1, 2];
	
	function GraficoMultController() {
		
		$.each(graficos, function (i, grafico) {
	        $.getJSON('http://localhost:8080/acciones/nemosporid/' + grafico,    function (data) {

	        	seriesOptions[i] = {
	                name: grafico,
	                data: JSON.parse(data.detalleNemo)
	            };

	            // As we're loading the data asynchronously, we don't know what order it will arrive. So
	            // we keep a counter and create the chart when all the data is loaded.
	            seriesCounter += 1;

	            if (seriesCounter === graficos.length) {
	                createChart();
	            }
	        });
	    });
	}
	
	
	function createChart() {

        $('#container_grafico').highcharts('StockChart', {

        	title : {
				text : "Grafico Multiple"
			},
        	
            rangeSelector: {
                selected: 4
            },

            yAxis: {
                labels: {
                    formatter: function () {
                        return (this.value > 0 ? ' + ' : '') + this.value + '%';
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },

            plotOptions: {
                series: {
                    compare: 'percent'
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2
            },

            series: seriesOptions
        });
    }
	
	
})();
