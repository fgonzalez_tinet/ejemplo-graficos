(function() {
    'use strict';

    angular.module('portabilidadEjecutivaApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('graficoMult', {
            parent: 'entity',
            url: '/graficoMult',
            data: {
                pageTitle: 'Graficos'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/grafico_multiple/grafico_multiple.html',
                    controller: 'GraficoMultController',
                    controllerAs: 'gmc'
                }
            }
        })
    }

})();
