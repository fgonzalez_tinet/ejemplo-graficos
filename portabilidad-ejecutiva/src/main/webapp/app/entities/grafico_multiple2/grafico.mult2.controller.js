(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller('GraficoMultController2',
			GraficoMultController2);
	
	var seriesOptions = [];
	
	function GraficoMultController2() {
        $.getJSON('http://localhost:8080/acciones/nemos/', function (nemos) {

        	$.each(nemos, function (i, grafico) {
	        	seriesOptions[i] = {
	                name: grafico.nombreNemo,
	                data: JSON.parse(grafico.detalleNemo)
	            };
	        });
        	createChart();
        });
	}
	
	
	function createChart() {

        $('#container_grafico').highcharts('StockChart', {

        	title : {
				text : "Grafico Multiple"
			},
        	
            rangeSelector: {
                selected: 4
            },

            yAxis: {
                labels: {
                    formatter: function () {
                        return (this.value > 0 ? ' + ' : '') + this.value + '%';
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },

            plotOptions: {
                series: {
                    compare: 'percent'
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2
            },

            series: seriesOptions
        });
    }
	
	
})();
