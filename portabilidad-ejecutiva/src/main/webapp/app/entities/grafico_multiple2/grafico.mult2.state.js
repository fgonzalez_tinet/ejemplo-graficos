(function() {
    'use strict';

    angular.module('portabilidadEjecutivaApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('graficoMult2', {
            parent: 'entity',
            url: '/graficoMult2',
            data: {
                pageTitle: 'Graficos'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/grafico_multiple2/grafico_multiple2.html',
                    controller: 'GraficoMultController2',
                    controllerAs: 'gmc2'
                }
            }
        })
    }

})();
