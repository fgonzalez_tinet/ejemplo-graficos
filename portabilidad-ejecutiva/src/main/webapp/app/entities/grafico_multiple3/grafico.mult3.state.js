(function() {
    'use strict';

    angular.module('portabilidadEjecutivaApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('graficoMult3', {
            parent: 'entity',
            url: '/graficoMult3',
            data: {
                pageTitle: 'Graficos'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/grafico_multiple3/grafico_multiple3.html',
                    controller: 'GraficoMultController3',
                    controllerAs: 'gmc3'
                }
            }
        })
    }

})();
