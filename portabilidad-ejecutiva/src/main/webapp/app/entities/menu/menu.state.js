(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('menu', {
			parent : 'entity',
			url : '/menu',
			data : {
				pageTitle : 'Menu Portabilidad'
			},
			views : {
				'content@' : {
					templateUrl : 'app/entities/menu/menu.html',
					controller : 'MenuController',
					controllerAs : 'vm'
				}
			}
		})
	}

})();
