(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller(
			'PuntoEditarController', PuntoEditarController);

	PuntoEditarController.$inject = [ '$rootScope', '$scope', '$stateParams',
			'entity', 'Punto', '$state'];

	function PuntoEditarController($rootScope, $scope, $stateParams, entity,
			Punto, $state) {
		var pec = this;
		pec.puntoEditar = entity;
		pec.editoCorrecto = false;
		pec.editoConError = false;
		$scope.serviceErrorMessage = {};

		var unsubscribe = $rootScope.$on(
				'portabilidadEjecutivaApp:puntoUpdate', function(event,
						result) {
					pec.puntoEditar = result;
				});

		$scope.$on('$destroy', unsubscribe);
		
		var onSuccess = function() {
			pec.editoCorrecto = true;
			pec.editoConError = false;
			hideLoading();
		};

		var onError = function(data) {
            $scope.serviceErrorMessage = data.headers('X-portabilidadEjecutivaApp-error');
            pec.editoConError = true;
            pec.editoCorrecto = false;
            hideLoading();
		};

		pec.editarPunto = function() {
				showLoading();
				pec.puntoEditar = Punto.update(pec.puntoEditar, onSuccess, onError);
		};		
	}
})();
