(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller(
			'PuntoNuevoController', PuntoNuevoController);

	PuntoNuevoController.$inject = [ '$rootScope', '$scope', '$stateParams', '$state', 'Punto' ];

	function PuntoNuevoController($rootScope, $scope, $stateParams, $state ,Punto) {
		var pnc = this;
		pnc.guardoCorrecto = false;
		pnc.guardoConError = false;
		pnc.puntoNuevo = {};
		$scope.serviceErrorMessage = {};

		var onSuccess = function() {
			$scope.serviceErrorMessage = {};
			pnc.guardoCorrecto = true;
			pnc.guardoConError = false;
			hideLoading();
		};

		var onError = function(data) {
            $scope.serviceErrorMessage = data.headers('X-portabilidadEjecutivaApp-error');
            pnc.guardoConError = true;
            pnc.guardoCorrecto = false;
            hideLoading();
		};

		pnc.guardar = function() {
			showLoading();
			Punto.save(pnc.puntoNuevo, onSuccess, onError);
		};

	}
})();
