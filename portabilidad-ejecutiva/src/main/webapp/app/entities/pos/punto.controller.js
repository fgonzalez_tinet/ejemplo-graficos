(function() {
	'use strict';

	angular.module('portabilidadEjecutivaApp').controller('PuntoController',
			PuntoController);

	PuntoController.$inject = [ '$rootScope', '$scope', '$state', 'Punto',
			'$document' ];

	function PuntoController($rootScope, $scope, $state, Punto, $document) {
		var pc = this;
		$scope.puntos = [];

		$scope.totalRecords = records();

		$scope.listarPuntos = function() {
			showLoading();
			Punto.list(function(result) {
				$scope.puntos = result;
				hideLoading();
			})
		};

		$scope.listarPuntos();

		function records() {
			var totalRecords = window.innerHeight;
			return Math.floor((totalRecords - 420) / 40);
		}
	}
})();
