(function() {
	'use strict';
	angular.module('portabilidadEjecutivaApp').factory('Punto', Punto);

	Punto.$inject = [ '$resource' ];

	function Punto($resource) {
		var resourceUrl = 'api/puntos/:id';

		return $resource(resourceUrl, {}, {
			'query' : { method : 'GET', isArray : true },
			'get' : { method : 'GET',
				transformResponse : function(data) {
					data = angular.fromJson(data);
					return data;
				}
			},
			'update' : { method : 'PUT'	},
			'list' : {
				url: 'api/puntos/administrador',
				method: 'GET',
				isArray: true
			}
		});
	}
})();
