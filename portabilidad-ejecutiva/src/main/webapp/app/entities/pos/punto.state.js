(function() {
    'use strict';

    angular.module('portabilidadEjecutivaApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('pos', {
            parent: 'entity',
            url: '/punto-venta',
            data: {
                pageTitle: 'Puntos de Venta'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pos/punto.html',
                    controller: 'PuntoController',
                    controllerAs: 'pc'
                }
            }
        }).state('punto-nuevo', {
            parent: 'pos',
            url: '/punto-nuevo',
            data: {
                pageTitle: 'Nuevo Punto Venta'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pos/punto-nuevo.html',
                    controller: 'PuntoNuevoController',
                    controllerAs: 'pnc'
                }
            }
        }).state('punto-editar', {
            parent: 'pos',
            url: '/punto-editar/{id}',
            data: {
                pageTitle: 'Editar Punto'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pos/punto-editar.html',
                    controller: 'PuntoEditarController',
                    controllerAs: 'pec'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Punto', function($stateParams, Punto) {
                    return Punto.get({id : $stateParams.id});
                }]
            }
        }).state('punto-ver', {
            parent: 'pos',
            url: '/punto-ver/{id}',
            data: {
                pageTitle: 'Ver detalle punto'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pos/punto-ver.html',
                    controller: 'PuntoVerController',
                    controllerAs: 'pvc'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Punto', function($stateParams, Punto) {
                    return Punto.get({id : $stateParams.id});
                }]
            }
        })
    }

})();
